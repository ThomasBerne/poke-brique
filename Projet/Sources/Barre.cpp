#include "Barre.h"



Barre::Barre()
{
	pos.x = SCREENSIZEX / 2;
	pos.y = SCREENSIZEY - 35;
	multiplicateur = 1.0f;
	etat = "normal";
}


Barre::~Barre()
{

}

void Barre::deplacementBarre()
{
	if (pos.x - 50.0f > 50.0f && KGetLeft()) {
		pos.x -= 15.0f;
	}
	if (pos.x + 50.0f < SCREENSIZEX && KGetRight()) {
		pos.x += 15.0f;
	}
}

void Barre::affichageBarre(int spriteBarre) {
	if (multiplicateur == 1.0f) {
		etat = "normal";
	}
	if (etat == "grand") {
		KSetDisplayColor(GREEN);
	}
	else if (etat == "petit") {
		KSetDisplayColor(RED);
	}
	KPrintSpriteCenter(spriteBarre, pos, 0.0f, (200.0f * multiplicateur) / 1000.0f);
	KSetDisplayColor(WHITE);
}

void Barre::reset()
{
	pos.x = SCREENSIZEX / 2;
	pos.y = SCREENSIZEY - 35;
	multiplicateur = 1.0f;
	etat = "normal";
}
