#pragma once

#include "Ngck_moteur/Ngck.h"
#include <stdlib.h>
#include <vector>
#include <time.h>

class Barre
{
public:
	f2d pos;
	int sprite;
	float multiplicateur;
	std::string etat;

	Barre();
	~Barre();
	void deplacementBarre();
	void affichageBarre(int spriteBarre);
	void reset();
};

