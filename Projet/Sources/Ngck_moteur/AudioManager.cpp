// -----------------------------------------------------------------
// Name : cAudioManager
// -----------------------------------------------------------------
#include "AudioManager.h"
#ifdef _DEBUG
#define DISPLAYINFO // Display info on played sounds
#endif
#include <io.h>         // _findfirst
#include <Windows.h> /// exit
#include <direct.h> // chdir

#undef PlaySound // In windows.h Playsound is a define

cAudioManager* cAudioManager::mInstance=NULL;

FMOD::System* fmodSystem = 0;

#define MAXSOUNDS 2048

#define MAXALTSOUNDS 20
struct sSound
{
  char           name[128];
  FMOD::Sound*   sound; // FMOD pointer to sound
  int            nbalt; // Number of total different sound . 1 to max
  FMOD::Sound*   soundsalt[MAXALTSOUNDS]; // Sound + alt (first one is same as sound)
  int            lastrandindexes[3]; // Keep track of last three index not to play always the same alternate sound
  bool           is2D; // 2d sound
  FMOD::Channel* lastchannelused;
  float          lastsoundlength; // Lenght of last played sound
  float          maxsoundlength;  // Maximum length of all sounds (with alternates sounds)
};

sSound gAllSounds[MAXSOUNDS];
int    mNbSounds=0;

void ERRCHECK(FMOD_RESULT result)
{
  if (result != FMOD_OK)
  {
    printf("FMOD error! (%d) %s. Press return to continue\n", result, FMOD_ErrorString(result));
    getchar();
    exit(-1);
  }
}

#define OCCSIZE 5.0f
#define OCCHALFSIZE (OCCSIZE/2.0f)

// Along x
FMOD_VECTOR occludervertices[3]= // Y up
{ { -OCCHALFSIZE , -0.5f  , 0 }  ,
{ 0            , OCCSIZE, 0 }  ,
{ OCCHALFSIZE  , -0.5f  , 0 }  ,
};

#define X1 22.5f
#define X2 29.4f
#define Z1 8.4f
#define Z2 14.0f

FMOD_VECTOR occludervertices21[4]= // ROOM10 WALL1
{ 
  { X1 , -0.5f  , Z1 }  ,
  { X2 , -0.5f  , Z1 }  ,
  { X2 ,  0.5f  , Z1 }  ,
  { X1 ,  0.5f  , Z1 }
};

FMOD_VECTOR occludervertices22[4]= // ROOM10 WALL1
{ 
  { X2 , -0.5f  , Z1 }  ,
  { X2 , -0.5f  , Z2 }  ,
  { X2 ,  0.5f  , Z2 }  ,
  { X2 ,  0.5f  , Z1 }
};

FMOD_VECTOR occludervertices23[4]= // ROOM10 WALL1
{ 
  { X1 , -0.5f  , Z1 }  ,
  { X1 , -0.5f  , Z2 }  ,
  { X1 ,  0.5f  , Z2 }  ,
  { X1 ,  0.5f  , Z1 }
};


FMOD_VECTOR occludervertices24[4]= // ROOM10 WALL1
{ 
  { X2 , -0.5f  , Z2 }  ,
  { X1 , -0.5f  , Z2 }  ,
  { X1 ,  0.5f  , Z2 }  ,
  { X2 ,  0.5f  , Z2 }
};

// -----------------------------------------------------------------
// Name : cAudioManager
// -----------------------------------------------------------------
cAudioManager::cAudioManager()
{
  // Init FMod
  FMOD_RESULT      result;
  unsigned int     version;
  FMOD_SPEAKERMODE speakermode;

  // Init sound array
  int i,j;
  for (i=0; i<MAXSOUNDS; i++)
  {
    gAllSounds[i].name[0]=0;
    gAllSounds[i].sound=NULL;
    gAllSounds[i].nbalt=0;
    for (j=0; j<MAXALTSOUNDS; j++)
      gAllSounds[i].soundsalt[j]=NULL;
    gAllSounds[i].lastrandindexes[0]=-1;
    gAllSounds[i].lastrandindexes[1]=-1;
    gAllSounds[i].lastrandindexes[2]=-1;
    gAllSounds[i].is2D=false;
    gAllSounds[i].lastsoundlength=0.0f;
    gAllSounds[i].maxsoundlength=0.0f;
  }

  // Create a System object and initialize.
  result = FMOD::System_Create(&fmodSystem);
  ERRCHECK(result);

  result = fmodSystem->getVersion(&version);
  ERRCHECK(result);

  if (version < FMOD_VERSION)
  {
    printf("Error!  You are using an old version of FMOD %08x.  This program requires %08x\n", version, FMOD_VERSION);
    exit(-1);
  }

  //result = fmodSystem->getDriverCaps(0, 0,0,0,&speakermode);
  //ERRCHECK(result);

  //result = fmodSystem->setSpeakerMode(speakermode);
  //ERRCHECK(result);

#define MAXCHANNEL 100

  result = fmodSystem->init(MAXCHANNEL, FMOD_INIT_NORMAL , 0); // FMOD_INIT_OCCLUSION_LOWPASS L'occlusion n'est plus ici TODO
  ERRCHECK(result);

  result = fmodSystem->setGeometrySettings(200.0f);
  ERRCHECK(result);

  // Create occluder

  fmodSystem->setGeometrySettings(100.0f);

  // Centered in 0,0. In Y axis (vertical), Size 2.0f in X, and 2.0f in Y
  //   mGeometry=NULL;
  //   result=fmodSystem->createGeometry(16,16, &mGeometry);
  //   ERRCHECK(result);
  //   int index;
  // #define OCCLUDERBACK_ATTENUATION 0.60f // 0.0 no attenuation 1.0f full occlusion (La malediction �tait a 0.75f)
  //   result = mGeometry->addPolygon(OCCLUDERBACK_ATTENUATION,OCCLUDERBACK_ATTENUATION,true,3,occludervertices,&index);
  //   ERRCHECK(result);

  //   for (i=0;i<MAXSOUNDWITHDELAY;i++)
  //   {
  //     mSoundWithDelayValues[i]=0.0f;
  //   }

  mNbSounds=0;

  FMOD::Reverb* reverb;
  fmodSystem->createReverb(&reverb);

  FMOD_REVERB_PROPERTIES ambientreverb = FMOD_PRESET_OFF; 
  result = fmodSystem->setReverbAmbientProperties(&ambientreverb);

  //#define FMOD_PRESET_OFF              // No effect
  //#define FMOD_PRESET_PADDEDCELL       // Tr�s faible
  //#define FMOD_PRESET_LIVINGROOM       // Tr�s faible
  //#define FMOD_PRESET_CARPETTEDHALLWAY // Faible
  //#define FMOD_PRESET_ROOM             // Piece taille normale
  //#define FMOD_PRESET_BATHROOM        // Tres grand
  //#define FMOD_PRESET_STONEROOM        // Raisonnance moyenne forte
  //#define FMOD_PRESET_AUDITORIUM       // Raisonnance forte

  //#define FMOD_PRESET_UNDERWATER      // Tr�s confine, resonnance grave
  //#define FMOD_PRESET_GENERIC         // Forte raisonnance
  //#define FMOD_PRESET_BATHROOM        // Tres grand

  //#define FMOD_PRESET_STONEROOM        // Raisonnance moyenne forte
  //#define FMOD_PRESET_AUDITORIUM       // Raisonnance forte

  //#define FMOD_PRESET_CONCERTHALL      {  0,  7,  1.00f, -1000,  -500,   0,   3.92f,  0.70f, 1.0f,  -1230, 0.020f,    -2, 0.029f, 0.25f, 0.000f, 5000.0f, 250.0f, 100.0f, 100.0f, 0x3f }
  //#define FMOD_PRESET_CAVE             {  0,  8,  1.00f, -1000,  0,      0,   2.91f,  1.30f, 1.0f,   -602, 0.015f,  -302, 0.022f, 0.25f, 0.000f, 5000.0f, 250.0f, 100.0f, 100.0f, 0x1f }
  //#define FMOD_PRESET_ARENA            {  0,  9,  1.00f, -1000,  -698,   0,   7.24f,  0.33f, 1.0f,  -1166, 0.020f,    16, 0.030f, 0.25f, 0.000f, 5000.0f, 250.0f, 100.0f, 100.0f, 0x3f }
  //#define FMOD_PRESET_HANGAR           {  0,  10, 1.00f, -1000,  -1000,  0,   10.05f, 0.23f, 1.0f,   -602, 0.020f,   198, 0.030f, 0.25f, 0.000f, 5000.0f, 250.0f, 100.0f, 100.0f, 0x3f }
  //#define FMOD_PRESET_HALLWAY          {  0,  12, 1.00f, -1000,  -300,   0,   1.49f,  0.59f, 1.0f,  -1219, 0.007f,   441, 0.011f, 0.25f, 0.000f, 5000.0f, 250.0f, 100.0f, 100.0f, 0x3f }
  //#define FMOD_PRESET_STONECORRIDOR    {  0,  13, 1.00f, -1000,  -237,   0,   2.70f,  0.79f, 1.0f,  -1214, 0.013f,   395, 0.020f, 0.25f, 0.000f, 5000.0f, 250.0f, 100.0f, 100.0f, 0x3f }
  //#define FMOD_PRESET_ALLEY            {  0,  14, 0.30f, -1000,  -270,   0,   1.49f,  0.86f, 1.0f,  -1204, 0.007f,    -4, 0.011f, 0.25f, 0.000f, 5000.0f, 250.0f, 100.0f, 100.0f, 0x3f }
  //#define FMOD_PRESET_FOREST           {  0,  15, 0.30f, -1000,  -3300,  0,   1.49f,  0.54f, 1.0f,  -2560, 0.162f,  -229, 0.088f, 0.25f, 0.000f, 5000.0f, 250.0f,  79.0f, 100.0f, 0x3f }
  //#define FMOD_PRESET_CITY             {  0,  16, 0.50f, -1000,  -800,   0,   1.49f,  0.67f, 1.0f,  -2273, 0.007f, -1691, 0.011f, 0.25f, 0.000f, 5000.0f, 250.0f,  50.0f, 100.0f, 0x3f }
  //#define FMOD_PRESET_MOUNTAINS        {  0,  17, 0.27f, -1000,  -2500,  0,   1.49f,  0.21f, 1.0f,  -2780, 0.300f, -1434, 0.100f, 0.25f, 0.000f, 5000.0f, 250.0f,  27.0f, 100.0f, 0x1f }
  //#define FMOD_PRESET_QUARRY           {  0,  18, 1.00f, -1000,  -1000,  0,   1.49f,  0.83f, 1.0f, -10000, 0.061f,   500, 0.025f, 0.25f, 0.000f, 5000.0f, 250.0f, 100.0f, 100.0f, 0x3f }
  //#define FMOD_PRESET_PLAIN            {  0,  19, 0.21f, -1000,  -2000,  0,   1.49f,  0.50f, 1.0f,  -2466, 0.179f, -1926, 0.100f, 0.25f, 0.000f, 5000.0f, 250.0f,  21.0f, 100.0f, 0x3f }
  //#define FMOD_PRESET_PARKINGLOT       {  0,  20, 1.00f, -1000,  0,      0,   1.65f,  1.50f, 1.0f,  -1363, 0.008f, -1153, 0.012f, 0.25f, 0.000f, 5000.0f, 250.0f, 100.0f, 100.0f, 0x1f }
  //#define FMOD_PRESET_SEWERPIPE        {  0,  21, 0.80f, -1000,  -1000,  0,   2.81f,  0.14f, 1.0f,    429, 0.014f,  1023, 0.021f, 0.25f, 0.000f, 5000.0f, 250.0f,  80.0f,  60.0f, 0x3f }

  //   mGeometry2=NULL;
  //   result=fmodSystem->createGeometry(16,16, &mGeometry2);
  //   ERRCHECK(result);
  //   mGeometry2->addPolygon(1.00f,1.00f,true,4,occludervertices21,&index);
  //   mGeometry2->addPolygon(1.00f,1.00f,true,4,occludervertices22,&index);
  //   mGeometry2->addPolygon(1.00f,1.00f,true,4,occludervertices23,&index);
  //   mGeometry2->addPolygon(1.00f,1.00f,true,4,occludervertices24,&index);
  //   mGeometry2->setActive(true);
}

// -----------------------------------------------------------------
// Name : 
// -----------------------------------------------------------------
// Return error (true = error)
bool cAudioManager::LoadSound(char* name, int currentindex, bool flagloop, bool flag2d)
{
  FMOD_RESULT result;

  // If first time an objet is loaded, then duplicate it in the first slot of alternates
  if ( gAllSounds[currentindex].nbalt == 0 )
  {
    if ( flag2d )
    {
      result = fmodSystem->createSound(name, FMOD_SOFTWARE, 0, &(gAllSounds[currentindex].sound) );
      gAllSounds[currentindex].is2D=true;
    }
    else
    {
      result = fmodSystem->createSound(name, FMOD_SOFTWARE | FMOD_3D, 0, &(gAllSounds[currentindex].sound) );
      gAllSounds[currentindex].is2D=false;
    }

    ERRCHECK(result);
    result = gAllSounds[currentindex].sound->set3DMinMaxDistance(1.0f, 10000.0f);
    ERRCHECK(result);
    if ( flagloop )
      result = gAllSounds[currentindex].sound->setMode(FMOD_LOOP_NORMAL);
    ERRCHECK(result);
    gAllSounds[currentindex].nbalt = 1;
    gAllSounds[currentindex].soundsalt[0]=gAllSounds[currentindex].sound;
    char tempname[128]; // Remove extension 
    char* pTmp;
    strcpy(tempname,name);
    pTmp = strrchr(tempname,'.');
    if (pTmp)
    {
      pTmp[0]=0;
    }
    // Remove flags
    pTmp = strchr(tempname,'_'); // Cut after first flag
    if (pTmp)
    {
      pTmp[0]=0;
    }
    strcpy(gAllSounds[currentindex].name, tempname);
    // Add max length
    unsigned int duration_ms;
    float duration;
    gAllSounds[currentindex].sound->getLength( &duration_ms, FMOD_TIMEUNIT_MS);
    duration = ((float)duration_ms)/1000.0f;
    if ( duration > gAllSounds[currentindex].maxsoundlength )
      gAllSounds[currentindex].maxsoundlength = duration;
  }
  else
  {
    // Add alternate sound
    int index;
    index = gAllSounds[currentindex].nbalt;
    result = fmodSystem->createSound(name, FMOD_SOFTWARE | FMOD_3D, 0, &(gAllSounds[currentindex].soundsalt[index]) );
    ERRCHECK(result);
    result = gAllSounds[currentindex].soundsalt[index]->set3DMinMaxDistance(1.0f, 10000.0f);
    ERRCHECK(result);
    gAllSounds[currentindex].nbalt++; // TODO Check max

    // Add max length
    unsigned int duration_ms;
    float duration;
    gAllSounds[currentindex].soundsalt[index]->getLength( &duration_ms, FMOD_TIMEUNIT_MS);
    duration = ((float)duration_ms)/1000.0f;
    if ( duration > gAllSounds[currentindex].maxsoundlength )
      gAllSounds[currentindex].maxsoundlength = duration;
  }

  return false;
}

// -----------------------------------------------------------------
// Name : Init
// data/Voice
// -----------------------------------------------------------------
void cAudioManager::NameGetFlags(char* name, bool* flagalt, bool* flagloop, bool* flag2D)
{
  *flagalt=false;
  *flagloop=false;
  *flag2D=false;
  if ( strstr( name, "_ALT" ) != NULL )
    *flagalt=true;
  if ( strstr( name, "_LOOP" ) != NULL )
    *flagloop=true;
  if ( strstr( name, "_2D" ) != NULL )
    *flag2D=true;
}

// -----------------------------------------------------------------
// Name : IsValidSoundFile
// -----------------------------------------------------------------
bool cAudioManager::IsValidSoundFile(char* name)
{
  char* pExt;
  pExt = strrchr(name, '.');
  if ( pExt )
  {
    if ( stricmp(pExt, ".ogg") == 0 )
      return true;
    else if ( stricmp(pExt, ".wav") == 0 )
      return true;
    else if ( stricmp(pExt, ".mp3") == 0 )
      return true;
  }
  return false;
}


// -----------------------------------------------------------------
// Name : Init
// data/Voice
// -----------------------------------------------------------------
void cAudioManager::Init()
{
  // Init level

#ifdef NOAUDIO
  return;
#endif

  bool flagalt;
  bool flagloop;
  bool flag2d;

  int currentindex;
  currentindex=0;

  long        lFileHandle;
  _finddata_t stFileInfo;

  _chdir("datas");
  _chdir("audio");

  // Play sound "Loading game, please wait"
  //LoadSound("VOV038-INTRO-LOADING_2D.ogg",currentindex,false,true);
  //currentindex++;
  //mNbSounds++;
  //PlaySound("VOV038-INTRO-LOADING");

  // -- Parse all file, first pass (no alternate)
  lFileHandle = _findfirst( "*", &stFileInfo );
  if ( lFileHandle > 0 )
  {
    do
    { 
      bool isvalidsoundtype; 

      NameGetFlags(stFileInfo.name,&flagalt,&flagloop,&flag2d);

      isvalidsoundtype = IsValidSoundFile( stFileInfo.name );

      if ( strcmp( "desktop.ini", stFileInfo.name ) == 0 )
      {
      }
      else if ( stFileInfo.attrib & _A_SUBDIR )
      {
      }
      else if ( isvalidsoundtype )
      {
        //printf("%s\n", stFileInfo.name);
        bool error;
        // Check if sound have "_ALT" flag
        if ( flagalt )
        {
          // First pass, do not process the alternate files
        }
        else
        {
          error = LoadSound(stFileInfo.name,currentindex,flagloop,flag2d);
          if ( !error )
          {
            currentindex++;
            mNbSounds++;
          }
        }
      }
    }
    while(_findnext( lFileHandle, &stFileInfo ) >= 0);
  }
  // Parse again all find, then check for alternates
  lFileHandle = _findfirst( "*", &stFileInfo );
  if ( lFileHandle > 0 )
  {
    do
    { 
      NameGetFlags(stFileInfo.name,&flagalt,&flagloop,&flag2d);
      // If directory then parse it
      if ( strcmp( "desktop.ini", stFileInfo.name ) == 0 )
      {
      }
      else if ( stFileInfo.attrib & _A_SUBDIR )
      {
      }
      else
      {
        bool error;
        // Check if sound have "_ALT" flag
        if ( flagalt )
        {
          // First pass, do not process the alternate files
          int index;
          char name[128];
          // Remove flags
          strcpy(name, stFileInfo.name);
          char* pTmp;
          //char* pTmpExt;
          pTmp = strchr(name,'_');
          //pTmpExt = strrchr(name,'.');
          if ( pTmp )
          {
            pTmp[0]=0;
            index = FindIndexFromName(name);
            LoadSound(stFileInfo.name,index,flagloop,flag2d);
          }

        }
        else
        {
        }
      }
    }
    while(_findnext( lFileHandle, &stFileInfo ) >= 0);
  }

  chdir("..");
  chdir("..");
}

// -----------------------------------------------------------------
// Name : ~cAudioManager
// -----------------------------------------------------------------
cAudioManager::~cAudioManager()
{
}

// -----------------------------------------------------------------
// Name : Update
// -----------------------------------------------------------------
void cAudioManager::Update(float delta)
{
  FMOD_RESULT result;

  // Check for delayed sound to be played
  //   int i;
  //   for (i=0;i<MAXSOUNDWITHDELAY;i++)
  //   {
  //     if ( mSoundWithDelayValues[i] > 0.0f )
  //     {
  //       mSoundWithDelayValues[i] -= delta;
  //       if ( mSoundWithDelayValues[i] <= 0.0f )
  //       {
  //         PlaySound( mSoundWithDelayName[i], mSoundWithDelayPositions[i] );
  //       }
  //     }
  //   }

  // hero position = mListenerPos
  // hero orientation = mListenerOrientation

  // Update listener
  FMOD_VECTOR listenerVector;
  listenerVector.x = mListenerPos.x;
  listenerVector.y = 0;
  listenerVector.z = mListenerPos.y;

  static FMOD_VECTOR lastpos = { 0.0f, 0.0f, 0.0f };
  static bool bFirst = true;
  FMOD_VECTOR forward;
  FMOD_VECTOR up;
  FMOD_VECTOR vel;

  forward.x = mListenerOrientation.x;
  forward.y = 0;
  forward.z = mListenerOrientation.y;
  up.x = 0;
  up.y = 1.0f;
  up.z = 0;

  // ********* NOTE ******* READ NEXT COMMENT!!!!!
  // vel = how far we moved last FRAME (m/f), then time compensate it to SECONDS (m/s).
  vel.x = (listenerVector.x - lastpos.x) * delta;
  vel.y = (listenerVector.y - lastpos.y) * delta;
  vel.z = (listenerVector.z - lastpos.z) * delta;
  if (bFirst)
  {
    bFirst = false;
    vel.x = 0;
    vel.y = 0;
    vel.z = 0;
  }

  static FMOD_VECTOR lastVel = { 0.0f, 0.0f, 0.0f };

  // store pos for next time
  lastpos = listenerVector;
  lastVel = vel;

  result = fmodSystem->set3DListenerAttributes(0, &listenerVector, &vel, &forward, &up);
  ERRCHECK(result);

  // Move Occluder
  // Same rotation as player, always behind him

#define OCCDISTBEHINDPLAYER 0.5f

  /*
  forward.x=0.0f;
  forward.y=0.0f;
  forward.z=1.0f;
  */

  //   FMOD_VECTOR occluderpos;
  //   FMOD_VECTOR forward1;
  //   forward1.x = forward.x*OCCDISTBEHINDPLAYER;
  //   forward1.y = forward.y*OCCDISTBEHINDPLAYER;
  //   forward1.z = forward.z*OCCDISTBEHINDPLAYER;
  //   occluderpos.x = listenerVector.x - forward1.x;
  //   occluderpos.y = listenerVector.y - forward1.y;
  //   occluderpos.z = listenerVector.z - forward1.z;
  //   mGeometry->setPosition(&occluderpos);

  // Compute orientation to fit hero motion
  FMOD_VECTOR forward2;
  forward2.x = forward.x;
  forward2.y = 0.0f;
  forward2.z = -forward.z; // This is the trick to have good orientation

  //mGeometry->setRotation(&forward2, &up);

  result = fmodSystem->update();
  ERRCHECK(result);
}

// -----------------------------------------------------------------
// Name :
// -----------------------------------------------------------------
void  cAudioManager::SetListenerPos(f2d pos)
{
  mListenerPos=pos;
}

// -----------------------------------------------------------------
// Name :
// -----------------------------------------------------------------
void  cAudioManager::SetListenerOrientation(f2d pos)
{
  mListenerOrientation=pos;
}

// -----------------------------------------------------------------
// Name :
// -----------------------------------------------------------------
int cAudioManager::FindIndexFromName(char* name)
{
  int i;
  for (i=0; i<MAXSOUNDS; i++)
  {
    if ( stricmp( gAllSounds[i].name,name)==0)
      return i;
  }
  return -1;
}

// -----------------------------------------------------------------
// Name :
// -----------------------------------------------------------------
FMOD::Channel* cAudioManager::PlaySound( char* name, f2d _pos, float volume )
{
  FMOD_RESULT result;

  int index;
  index = FindIndexFromName(name);

  if ( index == -1 )
  {
    printf("Error : Can not find sound : %s\n", name);
    return NULL;
  }

#ifdef DISPLAYINFO
  printf("PlaySound %s index %d\n", name, index); // Debug
#endif

#ifdef NOAUDIO
  printf("PlaySound %s index %d\n", name, index);
  return NULL;
#endif

  if ( volume > 1.0f )
    volume = 1.0f;
  if ( volume < 0.0f )
    volume = 0.0f;

  // play object sounds
  FMOD_VECTOR pos = { _pos.x, 0.0f, _pos.y };
  FMOD_VECTOR vel = { 0.0f,  0.0f, 0.0f };

  FMOD::Channel* channel;

  if ( gAllSounds[index].nbalt == 1 )
  {
    result = fmodSystem->playSound( FMOD_CHANNEL_FREE, gAllSounds[index].sound, true, &channel);

    unsigned int duration_ms;
    float duration;
    gAllSounds[index].sound->getLength( &duration_ms, FMOD_TIMEUNIT_MS);
    gAllSounds[index].lastsoundlength = ((float)duration_ms)/1000.0f;
  }
  else
  {
    int altrand;
    altrand = KRandInt(0,gAllSounds[index].nbalt-1);
    // Test if last sound was the same, else try again
    if ( altrand == gAllSounds[index].lastrandindexes[0] )
      altrand = KRandInt(0,gAllSounds[index].nbalt-1);
    if ( altrand == gAllSounds[index].lastrandindexes[0] )
      altrand = KRandInt(0,gAllSounds[index].nbalt-1);

    result = fmodSystem->playSound( FMOD_CHANNEL_FREE, gAllSounds[index].soundsalt[altrand], true, &channel);
    // Remember last rand sound (not to play it again
    gAllSounds[index].lastrandindexes[0] = altrand;

    unsigned int duration_ms;
    float duration;
    gAllSounds[index].soundsalt[altrand]->getLength( &duration_ms, FMOD_TIMEUNIT_MS);
    gAllSounds[index].lastsoundlength = ((float)duration_ms)/1000.0f;
  }
  ERRCHECK(result);
  if ( !gAllSounds[index].is2D )
  {
    result = channel->set3DAttributes(&pos, &vel);
    ERRCHECK(result);
    result = channel->set3DSpread(40.0f);
    ERRCHECK(result);
  }
  result = channel->setVolume(volume);
  ERRCHECK(result);
  result = channel->setPaused(false);
  ERRCHECK(result);

  gAllSounds[index].lastchannelused=channel;

  return channel; // To control position
}

// -----------------------------------------------------------------
// Name :
// -----------------------------------------------------------------
FMOD::Channel* cAudioManager::PlaySound( char* name )
{
  FMOD::Channel* channel;
  float volume = 1.0f;
  f2d _pos;
  _pos = mListenerPos;
  channel = PlaySound(name, _pos, volume );
  return channel;
}

// -----------------------------------------------------------------
// Name : GetSoundLength
// Always requested after "Playsound".
// So for random sound, we know which sound have been played
// Only work for sound that have been played already.
// For delayed sound, check function "GetSoundLengthWithDelay"
// -----------------------------------------------------------------
float cAudioManager::GetSoundLength( char* name )
{
  int index;
  index = FindIndexFromName(name);
  if ( index == -1 )
    return 0.0f;

  float duration;
  duration = gAllSounds[index].lastsoundlength; // Length have been stored here when sound have been played
  return duration;

  //unsigned int duration_ms;
  //float duration;
  //gAllSounds[index].sound->getLength( &duration_ms, FMOD_TIMEUNIT_MS);
  //duration = ((float)duration_ms)/1000.0f;
  //return duration;
}

// -----------------------------------------------------------------
// Name : GetSoundLengthWithDelay
// Check the longest sound value for all alternative.
// The sound have been batched, but not played yet (so not random have been done).
// So we can not know exactly which sound (and length will be played)
// -----------------------------------------------------------------
float cAudioManager::GetSoundLengthWithDelay( char* name )
{
  int index;
  index = FindIndexFromName(name);
  if ( index == -1 )
    return 0.0f;
  float duration;
  duration = gAllSounds[index].maxsoundlength; // Precomputed value
  return duration;
}

// -----------------------------------------------------------------
// Name :
// -----------------------------------------------------------------
void cAudioManager::PlaySoundWithDelay(char* name, f2d pos, float delay)
{
  // Find free slot
  //   int i;
  //   for (i=0;i<MAXSOUNDWITHDELAY;i++)
  //   {
  //     if ( mSoundWithDelayValues[i] <= 0.0f )
  //       break;
  //   }
  //   // Found
  //   if ( i < MAXSOUNDWITHDELAY )
  //   {
  //     mSoundWithDelayValues[i]=delay;
  //     strcpy( mSoundWithDelayName[i], name );
  //     mSoundWithDelayPositions[i] = pos;
  //   }
}

// -----------------------------------------------------------------
// Name :
// -----------------------------------------------------------------
int  cAudioManager::GetNbOfsounds()
{
  return mNbSounds;
}

// -----------------------------------------------------------------
// Name :
// -----------------------------------------------------------------
char*  cAudioManager::GetSoundName(int index)
{
  return gAllSounds[index].name;
}

// -----------------------------------------------------------------
// Name :
// -----------------------------------------------------------------
void cAudioManager::StopSound( char* name )
{
  int index;
  index = FindIndexFromName(name);
  if ( index == -1 )
    return;
  gAllSounds[index].lastchannelused->stop();
}

// -----------------------------------------------------------------
// Name : 
// -----------------------------------------------------------------
void cAudioManager::SetReverbLevel( int level )
{
  //                                                                                       Inst Env  Diffus  Room   RoomHF  RmLF DecTm   DecHF  DecLF   Refl  RefDel   Revb  RevDel  ModTm  ModDp   HFRef    LFRef   Diffus  Densty  FLAGS */
  FMOD_REVERB_PROPERTIES ambientreverb0 = FMOD_PRESET_OFF;
  FMOD_REVERB_PROPERTIES ambientreverb1 = FMOD_PRESET_PADDEDCELL; // Small
  FMOD_REVERB_PROPERTIES ambientreverb2 = FMOD_PRESET_LIVINGROOM; // Normal room       // {  0,  4,  1.00f, -1000,  -6000,  0,   0.50f,  0.10f, 1.0f,  -1376, 0.003f, -1104, 0.004f, 0.25f, 0.000f, 5000.0f, 250.0f, 100.0f, 100.0f, 0x3f }
  FMOD_REVERB_PROPERTIES ambientreverb3 = FMOD_PRESET_CARPETTEDHALLWAY; // Normal room // {  0,  11, 1.00f, -1000,  -4000,  0,   0.30f,  0.10f, 1.0f,  -1831, 0.002f, -1630, 0.030f, 0.25f, 0.000f, 5000.0f, 250.0f, 100.0f, 100.0f, 0x3f }
  FMOD_REVERB_PROPERTIES ambientreverb4 = FMOD_PRESET_ROOM; // Small room, heavy reverb //{  0,  2,  1.00f, -1000,  -454,   0,   0.40f,  0.83f, 1.0f,  -1646, 0.002f,    53, 0.003f, 0.25f, 0.000f, 5000.0f, 250.0f, 100.0f, 100.0f, 0x3f }
  FMOD_REVERB_PROPERTIES ambientreverb5 = FMOD_PRESET_BATHROOM;//Large room,heavy reverb//{  0,  3,  1.00f, -1000,  -1200,  0,   1.49f,  0.54f, 1.0f,   -370, 0.007f,  1030, 0.011f, 0.25f, 0.000f, 5000.0f, 250.0f, 100.0f,  60.0f, 0x3f }
  FMOD_REVERB_PROPERTIES ambientreverb6 = FMOD_PRESET_STONEROOM; // Large cave. Too big //{  0,  5,  1.00f, -1000,  -300,   0,   2.31f,  0.64f, 1.0f,   -711, 0.012f,    83, 0.017f, 0.25f, 0.000f, 5000.0f, 250.0f, 100.0f, 100.0f, 0x3f }
  FMOD_REVERB_PROPERTIES ambientreverb7 = FMOD_PRESET_AUDITORIUM; // very large

  FMOD_REVERB_PROPERTIES ambientreverb;

  if (level==0)
    ambientreverb = ambientreverb0;
  else if (level==1)
    ambientreverb = ambientreverb1;
  else if (level==2)
    ambientreverb = ambientreverb2;
  else if (level==3)
    ambientreverb = ambientreverb3;
  else if (level==4)
    ambientreverb = ambientreverb4;
  else if (level==5)
    ambientreverb = ambientreverb5;
  else if (level==6)
    ambientreverb = ambientreverb6;
  else if (level==7)
    ambientreverb = ambientreverb7;

  //#define FMOD_PRESET_OFF              // No effect
  //#define FMOD_PRESET_PADDEDCELL       // Tr�s faible
  //#define FMOD_PRESET_LIVINGROOM       // Tr�s faible
  //#define FMOD_PRESET_CARPETTEDHALLWAY // Faible
  //#define FMOD_PRESET_ROOM             // Piece taille normale
  //#define FMOD_PRESET_BATHROOM        // Tres grand
  //#define FMOD_PRESET_STONEROOM        // Raisonnance moyenne forte
  //#define FMOD_PRESET_AUDITORIUM       // Raisonnance forte

  fmodSystem->setReverbAmbientProperties(&ambientreverb);
}

// -----------------------------------------------------------------
// Name : 
// -----------------------------------------------------------------
void cAudioManager::PauseAllSounds()
{
  int i;
  for (i=0; i<mNbSounds; i++)
  {
    if ( gAllSounds[i].lastchannelused )
    {
      bool paused;
      gAllSounds[i].lastchannelused->getPaused(&paused);
      if ( paused == false )
        gAllSounds[i].lastchannelused->setPaused(true);
    }
  }
}

// -----------------------------------------------------------------
// Name : 
// -----------------------------------------------------------------
void cAudioManager::UnPauseAllSounds()
{
  int i;
  for (i=0; i<mNbSounds; i++)
  {
    if ( gAllSounds[i].lastchannelused )
    {
      bool paused;
      gAllSounds[i].lastchannelused->getPaused(&paused);
      if ( paused == true )
        gAllSounds[i].lastchannelused->setPaused(false);
    }
  }
}

/*
// -----------------------------------------------------------------
// Name : 
// -----------------------------------------------------------------
void cAudioManager::Load3DSound(FMOD::Sound** sound, char* name,  bool loop)
{
FMOD_RESULT      result;
result = fmodSystem->createSound(name, FMOD_SOFTWARE | FMOD_3D, 0, sound);
ERRCHECK(result);
result = (*sound)->set3DMinMaxDistance(1.0f, 10000.0f);
ERRCHECK(result);
if ( loop )
result = (*sound)->setMode(FMOD_LOOP_NORMAL);

ERRCHECK(result);
}

// -----------------------------------------------------------------
// Name : 
// -----------------------------------------------------------------
void cAudioManager::Load2DSound(FMOD::Sound** sound, char* name,  bool loop)
{
FMOD_RESULT      result;
result = fmodSystem->createSound(name, FMOD_SOFTWARE, 0, sound);
ERRCHECK(result);
if ( loop )
result = (*sound)->setMode(FMOD_LOOP_NORMAL);
ERRCHECK(result);
}

static FMOD::Channel* channelambiance=NULL;

// -----------------------------------------------------------------
// Name : PlayAmbiance. 
// Game ambiances
// -----------------------------------------------------------------
void cAudioManager::PlayAmbiance(int id)
{
FMOD_RESULT result;

if ( ambiances[id-1] == NULL ) // Sound not loaded
return;

#ifdef NOAUDIO
printf("PlayAmbiance %d\n", id);
return;
#endif

if ( channelambiance )
{
channelambiance->stop();
}
// play object sounds
// TODO
#if 0
result = fmodSystem->playSound( ambiances[id-1], NULL, true, &channelambiance);
ERRCHECK(result);
result = channelambiance->setPaused(false);
ERRCHECK(result);

result = channelambiance->setVolume(0.5f);
ERRCHECK(result);
#endif
}

static FMOD::Channel* channelambiance2=NULL;

// -----------------------------------------------------------------
// Name : PlayAmbiance. 
// Specific ambiance, for hero (heart beating)
// -----------------------------------------------------------------
void cAudioManager::PlayAmbiance2(int id)
{
FMOD_RESULT result;

if ( ambiances[id-1] == NULL ) // Sound not loaded
return;

#ifdef NOAUDIO
printf("PlayAmbiance2 %d\n", id);
return;
#endif

if ( channelambiance2 )
{
channelambiance2->stop();
}
// play object sounds
// TODO
#if 0
result = fmodSystem->playSound( //FMOD_CHANNEL_FREE, ambiances[id-1], NULL, true, &channelambiance2);
ERRCHECK(result);
result = channelambiance2->setPaused(false);
ERRCHECK(result);
result = channelambiance2->setVolume(1.0f);
ERRCHECK(result);
#endif
}

// -----------------------------------------------------------------
// Name :
// Stop all sounds
// -----------------------------------------------------------------
void  cAudioManager::StopAmbiances()
{
if ( channelambiance )
{
channelambiance->stop();
}

if ( channelambiance2 )
{
channelambiance2->stop();
}

// Stop all channels
int i;
for (i=0; i<MAXCHANNEL; i++)
{
FMOD::Channel* pChannel;
fmodSystem->getChannel(i, &pChannel );
if ( pChannel )
pChannel->stop();
}
}


// -----------------------------------------------------------------
// Name : 
// -----------------------------------------------------------------
void cAudioManager::PlayMusics(int id)
{
FMOD_RESULT result;
static FMOD::Channel* channelmusics=NULL;

if ( musics[id-1] == NULL ) // Sound not loaded
return;

#ifdef NOAUDIO
printf("PlayMusics %d\n", id);
return;
#endif

if ( channelmusics )
{
channelmusics->stop();
}
// play object sounds
// 
#if 0
result = fmodSystem->playSound( //FMOD_CHANNEL_FREE, musics[id-1], NULL,true, &channelmusics);
ERRCHECK(result);
result = channelmusics->setPaused(false);
ERRCHECK(result);

result = channelmusics->setVolume(0.1f);
ERRCHECK(result);
#endif
}

static FMOD::Channel* channelvoices=NULL;

// -----------------------------------------------------------------
// Name : 
// -----------------------------------------------------------------
void cAudioManager::PlayVoices(int id)
{
FMOD_RESULT result;

if ( voices[id-1] == NULL ) // Sound not loaded
return;

#ifdef NOAUDIO
printf("PlayVoices %d\n", id);
return;
#endif

if ( channelvoices )
{
channelvoices->stop();
}
// play object sounds
// TODO
#if 0
result = fmodSystem->playSound( //FMOD_CHANNEL_FREE, voices[id-1], NULL, true, &channelvoices);
ERRCHECK(result);
result = channelvoices->setPaused(false);
ERRCHECK(result);
#endif
}

// -----------------------------------------------------------------
// Name : 
// -----------------------------------------------------------------
bool cAudioManager::IsVoiceFinished()
{
if ( channelvoices )
{
bool isplaying;
channelvoices->isPlaying(&isplaying);
return !isplaying;
}
else
return true;
}

// -----------------------------------------------------------------
// Name : 
// -----------------------------------------------------------------
FMOD::Channel* cAudioManager::PlaySound(int id, f2d _pos)
{
FMOD_RESULT result;

if ( sounds[id-1] == NULL ) // Sound not loaded
return NULL;

#ifdef NOAUDIO
printf("PlaySound3D %d (pos %f %f)\n", id,_pos.x,_pos.y);
return NULL;
#endif

// play object sounds
FMOD_VECTOR pos = { _pos.x, 0.0f, _pos.y };
FMOD_VECTOR vel = { 0.0f,  0.0f, 0.0f };

FMOD::Channel* channel;

// TODO
#if 0
result = fmodSystem->playSound( //FMOD_CHANNEL_FREE, sounds[id-1], NULL, true, &channel);
ERRCHECK(result);
result = channel->set3DAttributes(&pos, &vel);
ERRCHECK(result);
result = channel->set3DSpread(40.0f);
ERRCHECK(result);
result = channel->setPaused(false);
ERRCHECK(result);
#endif

return channel; // To control position
}
*/



