// -----------------------------------------------------------------
// FONT Manager
// -----------------------------------------------------------------
#include "FontManager.h"

//#define USE_DOUBLE_SPACE // JP : Single space = Word separator, but not displayed , double space = one space displayed

cFontManager * cFontManager::mInstance = NULL;

// ---------------------------------------------------------------
// Name : ReadBinaryInt_BigEndian
// ---------------------------------------------------------------
int ReadBinaryInt_BigEndian(FILE* file)
{
  unsigned char* pTmp;
  int result;
  result=0;
  pTmp = (unsigned char*) &result;
  fread((void*)&pTmp[0], 1, 1, file);
  fread((void*)&pTmp[1], 1, 1, file);
  fread((void*)&pTmp[2], 1, 1, file);
  fread((void*)&pTmp[3], 1, 1, file);
  return result;
}

// ---------------------------------------------------------------
// Name : ReadBinaryShort_BigEndian
// ---------------------------------------------------------------
signed short ReadBinaryShort_BigEndian(FILE* file)
{
  signed short result;
  unsigned char* pTmp;
  result=0;
  pTmp = (unsigned char*) &result;
  fread((void*)&pTmp[0], 1, 1, file);
  fread((void*)&pTmp[1], 1, 1, file);
  return result;
}


// ---------------------------------------------------------------
// Name : fsReadBinaryChar_BigEndian
// ---------------------------------------------------------------
signed short ReadBinaryChar_BigEndian(FILE* file)
{
  signed short result;
  result = 0;
  fread((void*)&result, 1, 1, file);
  return result;
}


// -----------------------------------------------------------------
// Name :
// -----------------------------------------------------------------
cFontManager::cFontManager()
{
  for (int i = 0; i < MAX_FONTS; i++)
  {
    mAllFonts[i] = NULL;
  }

  mColorRed=1.0f;
  mColorGreen=1.0f;
  mColorBlue=1.0f;
  mAlpha=255.0f;

  mFontGlobalScale=1.0f;
}

// -----------------------------------------------------------------
// Name :
// -----------------------------------------------------------------
cFontManager::~cFontManager()
{
  for (int i = 0; i < MAX_FONTS; i++)
  {
    if ( mAllFonts[i] )
      delete mAllFonts[i];
  }
}

// -----------------------------------------------------------------
// Name : Init
// Call this once
// -----------------------------------------------------------------
void cFontManager::Init()
{
  // Load all game fonts
  // They will always stay into memory

  CreateFont(FONT_DEFAULT,"Datas/TestFonte1.fnt");
  CreateFont(FONT_ORANGEJUICE,"Datas/OrangeJuice.fnt");
  

  //mAllFontsTextures[FONT_INTERFACE]=texGetTexture("RE_Interface");

  // Fishie Fishie Font Adjust
  //mAllFonts[FONT_SERENA]->mXSpacing -= 2;
  //mAllFonts[FONT_SERENA]->mLineHeight -= 12;
  //mAllFonts[FONT_STONEHINGE_32]->mLineHeight -= 10;
  //mAllFonts[FONT_STONEHINGE_24]->mLineHeight -= 8;

  //printf("FontManager Initialized\n");
}

// -----------------------------------------------------------------
// Name : Destroy
// Call this once
// -----------------------------------------------------------------
void cFontManager::Destroy()
{
  unloadAll();
}

// -----------------------------------------------------------------
// Name :
// -----------------------------------------------------------------
void cFontManager::unloadAll()
{
  for (int i = 0; i < MAX_FONTS; i++)
  {
    if ( mAllFonts[i] )
    {
      delete mAllFonts[i];
      mAllFonts[i] = NULL;
    }
  }
}

int cFontManager::BMFont_FindBlock(int id, FILE* file)
{
  // File is composed of blocks.
  // Header = 4 bytes
  // Blocks = 1 byte for ID, 4 bytes for block size (not including first 5 bytes)
  fseek(file, 4, SEEK_SET);

  int currentid;
  int size;
  size=0;
  currentid=-1;

  while( !feof(file) && currentid!=id)
  {
    // Go to next block (or do not move for first one)
    fseek(file, size, SEEK_CUR); 

    currentid = ReadBinaryChar_BigEndian(file);
    size = ReadBinaryInt_BigEndian(file);
  }
  return size;
}

// -----------------------------------------------------------------
// Name : parseFont
// -----------------------------------------------------------------
bool cFontManager::CreateFont(int fontid, char* fontdefname)
{
  cFont* font;
  mAllFonts[fontid] = new cFont();
  font = mAllFonts[fontid];

  // Open file
  FILE* file;
  file = fopen(fontdefname,"rb");

  // BMFont 1.2 Binary format. Header should be "BMF" and 3 (v3 of binary file)
  unsigned char header[4];
  if (file)
  {
    fread(header,1,4,file);
    if ( header[0] != 'B' || header[1] != 'M' || header[2] != 'F' || header[3] != 3 )
    {
      printf("FontManager. Binary file %s do not have right file format\n", fontdefname);
      return false;
    }
  }

  if (file)
  {
    BMFont_FindBlock(1, file);

    font->mFontSize=   ReadBinaryShort_BigEndian(file); // Used ?
    fseek(file, 9, SEEK_CUR);
    font->mXSpacing=   ReadBinaryChar_BigEndian(file);

    BMFont_FindBlock(2, file);

    font->mLineHeight= ReadBinaryShort_BigEndian(file);
    fseek(file, 2, SEEK_CUR);    
    font->mWidth=      ReadBinaryShort_BigEndian(file);
    font->mHeight=     ReadBinaryShort_BigEndian(file);

    int namesize;
    namesize=BMFont_FindBlock(3, file); // Font name
    char FontName[256];
    fread(FontName,1,namesize,file);
    FontName[namesize]=0;
    font->mTextureId=KLoadSprite(FontName); // Should be PNG file for NGCK

    int nbchars;
    nbchars = BMFont_FindBlock(4, file);
    nbchars = nbchars / 20;

    // Parse all lines
    for(int i=0; i<nbchars; i++)
    {
      font->mChars[i].id=      ReadBinaryInt_BigEndian(file);
      font->mChars[i].x=       ReadBinaryShort_BigEndian(file);
      font->mChars[i].y=       ReadBinaryShort_BigEndian(file);
      font->mChars[i].Width=   ReadBinaryShort_BigEndian(file);
      font->mChars[i].Height=  ReadBinaryShort_BigEndian(file);
      font->mChars[i].XOffset= ReadBinaryShort_BigEndian(file);
      font->mChars[i].YOffset= ReadBinaryShort_BigEndian(file);
      font->mChars[i].XAdvance=ReadBinaryShort_BigEndian(file);
      fseek(file, 2, SEEK_CUR);

      // BMFONT 1.2
      // Example for "i"
      // For Chinese font, the space are naturally bad, so use character lenght to define better space.
      /*
      #define SPACE_BETWEENX_SMALL -2
      #define SPACE_BETWEENX_BIG   -18
      if ( font->mChars[i].id != 32 ) // Not for space
      {
      if ( font->mFontSize < 60 )
      {
      font->mChars[i].XOffset = SPACE_BETWEENX_SMALL;
      font->mChars[i].XAdvance = font->mChars[i].Width + SPACE_BETWEENX_SMALL*2;
      }
      else
      {
      font->mChars[i].XOffset = SPACE_BETWEENX_BIG;
      font->mChars[i].XAdvance = font->mChars[i].Width + SPACE_BETWEENX_BIG*2;
      }
      }
      else
      {
      font->mChars[i].XAdvance = font->mFontSize/3;
      }
      */
      font->mChars[i].isValid = false;
      if ( font->mChars[i].Width != 0 )
        font->mChars[i].isValid = true;
    }


    fclose(file);
  }
  else
  {
    //printf("ERROR : could not open file %s.\n", fontdefname);
    return false;
  }

  return true;
}

int cFontManager::strlen_utf8(char* text)
{
  int i,j;
  i=0;
  j=0;
  while (text[i])
  {
    if ((text[i] & 0xC0) != 0x80) // Count everything except the 0x10xxxxxx bytes
      j++;
    i++;
  }
  return j;
}

int cFontManager::getcode_utf8(char* text, int* size)
{
  int code;
  code = 0;
  *size=0;
  // UTF8
  // 1 to 4 bytes
  // 1 Byte
  // 0xxxxxxx 	1 octet codant 1 � 7 bits
  // 110xxxxx 10xxxxxx 	2 octets codant 8 � 11 bits
  // 1110xxxx 10xxxxxx 10xxxxxx 	3 octets codant 12 � 16 bits
  // 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx 	4 octets codant 17 � 21 bits
  if ( (text[0] & 0x80) == 0 )
  {
    // 1 Byte
    code = text[0];
    *size=1;
  }
  else if ( (text[0] & 0xE0) == 0xC0 )
  {
    // 2 Bytes
    code = ((text[0]&0x1F)<<6)+((text[1]&0x3F));
    *size=2;
  }
  else if ( (text[0] & 0xF0) == 0xE0 )
  {
    // 3 Bytes
    code = ((text[0]&0x1F)<<(6+6))+((text[1]&0x3F)<<6)+((text[2]&0x3F));
    *size=3;
  }
  else if ( (text[0] & 0xF8) == 0xF0 )
  {
    // 4 Bytes
    code = ((text[0]&0x1F)<<(6+6+6))+((text[1]&0x3F)<<(6+6))+((text[2]&0x3F)<<6)+((text[3]&0x3F));
    *size=4;
  }

  return code;
}

// -----------------------------------------------------------------
// -----------------------------------------------------------------
CharDescriptor* cFontManager::getchardescriptor(int fontid, int code)
{
  int i;
  i=0;

  while ( mAllFonts[fontid]->mChars[i].isValid && mAllFonts[fontid]->mChars[i].id != code && i<FONT_MAXCHAR)
    i++;

  if (i==FONT_MAXCHAR)
    return &(mAllFonts[fontid]->mChars[FONT_MAXCHAR-1]); // Last 'unused surely' character

  return &(mAllFonts[fontid]->mChars[i]);
}

// -----------------------------------------------------------------
// Name : drawString
// -----------------------------------------------------------------
void cFontManager::drawString(int fontid, int x, int y, char* text, int yStart, int yEnd)
{
  cFont* font = mAllFonts[fontid];
  int lenght;
  float currentx, currenty;
  unsigned char* mytext; // string is unsigned
  mytext = (unsigned char*) text;

  if (yStart < 0) // Scrolling
    yStart = 0;

  currentx = x;
  currenty = y;

  if (mytext==NULL)
    return;

  lenght=strlen_utf8((char*)mytext); // use a UTF8 function

  int currenttextpos;
  currenttextpos=0;

  for (int i=0; i<lenght; i++)
  {
    int currentcode;
    int size;
    currentcode = getcode_utf8( (char*) &mytext[currenttextpos], &size );
    currenttextpos += size; // 1 2 3 4 (UTF 8)

    if (currentcode=='\\') // line return
    {
      currentx = x;
      currenty+= (font->mLineHeight*mFontGlobalScale);
      continue; // do not display anything
    }

    CharDescriptor* curchar;
    curchar = getchardescriptor(fontid, currentcode );

#ifdef USE_DOUBLE_SPACE
    if (currentcode==' ' && mytext[currenttextpos]==' ' ) // Double space means "real space"
    {
      if ( curchar->XAdvance != 0 )
        currentx += curchar->XAdvance; // Add a lenght for space caracter (if existing)
      else
        currentx += ((font->mFontSize/3)*mFontGlobalScale); // Add defaut lenght
      currentx += (font->mXSpacing*mFontGlobalScale);
      currenttextpos++; // Skip second space
      lenght--; // Reduce total  number of caracters
      continue; // do not display anything
    }
    if (currentcode==' ') // Single space, jap = ignore char (it is just word separator)
    {
      continue; // Skip
    }
#else
    // Single space
    if (currentcode==' ' ) // Space, do not display anything
    {
      if ( curchar->XAdvance != 0 )
        currentx += (curchar->XAdvance*mFontGlobalScale); // Add a lenght for space caracter (if existing)
      else
        currentx += ((font->mFontSize/3)*mFontGlobalScale); // Add defaut lenght
      currentx += font->mXSpacing;
      continue; // do not display anything
    }
#endif

    if (curchar->isValid)
    {
      int yDecal = curchar->YOffset*mFontGlobalScale - yStart;
      int ustart, uend, vstart, vend;
      int yDisp;
      ustart = curchar->x;
      uend = curchar->x+curchar->Width;
      if (yDecal < 0 && (curchar->YOffset>=0) ) // Only for caracters with non negatif offset
      {
        yDisp = 0;
        vstart = -yDecal;
      }
      else
      {
        yDisp = yDecal;
        vstart = 0;
      }
      yDisp += currenty;
      vstart += curchar->y;
      vend = (yEnd >= 0 && yEnd - curchar->YOffset <= curchar->Height) ? curchar->y + yEnd - curchar->YOffset : curchar->y+curchar->Height;

      bool notdisplay;
      notdisplay=false;

      // Test if letter is very outside the screen, if yes, do not display it
      //       if ( 
      //         !sysIsPosOnScreen( vec2(currentx+curchar->XOffset, yDisp) )
      //         && !sysIsPosOnScreen( vec2(currentx+curchar->XOffset+(uend-ustart)*mFontGlobalScale, yDisp+(vend-vstart)*mFontGlobalScale) )
      //         )
      //       {
      //         notdisplay=true;
      //       }


      if ( !notdisplay )
      {
        KPrintSpriteWithUV( mAllFonts[fontid]->mTextureId, currentx+curchar->XOffset, yDisp, ustart, uend, vstart, vend, (uend-ustart)*mFontGlobalScale, (vend-vstart)*mFontGlobalScale );
      }

      currentx += ((curchar->XAdvance + font->mXSpacing)*mFontGlobalScale);
    }
  }
}

// -----------------------------------------------------------------
// Name : drawString
// -----------------------------------------------------------------
void cFontManager::drawString(int fontid, int x, int y, char* text)
{
  drawString(fontid, x, y, text, 0, -1);
}

// -----------------------------------------------------------------
// Name : drawStringCenter
// -----------------------------------------------------------------
void cFontManager::drawStringCenter(int fontid, int x, int y, char* text)
{
  int newx;
  newx = x - ((float)getStringLenght(fontid,(unsigned char*)text))/2.0f;
  drawString(fontid,newx,y,text);  
}

// -----------------------------------------------------------------
// Name : drawStringRight
// -----------------------------------------------------------------
void cFontManager::drawStringRight(int fontid, int x, int y, char* text)
{
  int newx;
  newx = x - getStringLenght(fontid,(unsigned char*)text);
  drawString(fontid,newx,y,text);  
}

// -----------------------------------------------------------------
// Name : drawStringInBox
// Draw words until box size is reached and then go to next line and continue display
// -----------------------------------------------------------------
void cFontManager::drawStringInBox(int fontid, int x, int y, char* _text, int lenght)
{
  cFont* font = mAllFonts[fontid];
  unsigned char stringtemp[512];
  unsigned char stringsmall[2];
  unsigned char* currenttext;
  int   currenty;
  int   currentsize;
  float nextwordsize;
  stringsmall[1]=0;
  currenty = y;
  currenttext=(unsigned char*)_text;

  // Fix for longuer texts
  float textmaxwidth;
  float savescale;
  savescale=mFontGlobalScale;
  textmaxwidth=getTextWidth(fontid,_text,lenght);
  if ( textmaxwidth > lenght )
  {
    float height;
    float heightafter;
    height = getTextHeight(fontid,_text,lenght);

    mFontGlobalScale= lenght/textmaxwidth;

    heightafter = getTextHeight(fontid,_text,lenght);
    currenty += ((height-heightafter) /2.0f);
  }

  // Add all words to temp string until the box size is reach
  while ( currenttext[0]!=0 )
  {
    currentsize=0;
    stringtemp[0]=0;
    nextwordsize=getWordLenght(fontid,currenttext);
    while(    ( (currentsize + nextwordsize < lenght) && (currenttext[0]!='\\') && (currenttext[0]!=0) )
      || (  currentsize==0 && nextwordsize>=lenght ) // If the whole word do not fit, this is not a problem, display it.
      )
    {

#ifdef USE_DOUBLE_SPACE
      // Copy next word to temp string
      if ( currenttext[0]==' ' && currenttext[1]==' ')
      {
        strcat((char*)stringtemp," ");
        currenttext+=2;
      }
      else if (currenttext[0]==' ')
      {
        currenttext++;
      }
#else
      if (currenttext[0]==' ')
      {
        strcat((char*)stringtemp," ");
        currenttext++;
      }
#endif
      while ( (currenttext[0]!=0) && (currenttext[0]!=' ') && (currenttext[0]!='\\') )
      {
        stringsmall[0]=currenttext[0];
        strcat((char*)stringtemp,(char*)stringsmall);
        currenttext++;
      }
      currentsize+=nextwordsize;
      nextwordsize=getWordLenght(fontid,currenttext);
    }
    // Display string
    drawString(fontid,x,currenty,(char*)stringtemp);
    // Go to next line
    currenty+= (font->mLineHeight*mFontGlobalScale);
    // If next caracter is special caracter then jump it
    if ( (currenttext[0]=='\\') || (currenttext[0]==' ') )
      currenttext++;
  }

  mFontGlobalScale= savescale;
}

// -----------------------------------------------------------------
// Name : drawStringCenterInBox
// Draw words until box size is reached and then go to next line and continue display
// -----------------------------------------------------------------
void cFontManager::drawStringCenterInBox(int fontid, int x, int y, char* _text, int lenght)
{
  cFont* font = mAllFonts[fontid];
  unsigned char stringtemp[512];
  unsigned char stringsmall[2];
  unsigned char* currenttext;
  int   currenty;
  int   currentsize;
  int   nextwordsize;
  stringsmall[1]=0;
  currenty = y;

  if ( _text==NULL)
    return;

  currenttext=(unsigned char*)_text;

  // Fix for longuer texts
  float textmaxwidth;
  float savescale;
  savescale=mFontGlobalScale;
  textmaxwidth=getTextWidth(fontid,_text,lenght);
  if ( textmaxwidth > lenght )
  {
    float height;
    float heightafter;
    height = getTextHeight(fontid,_text,lenght);

    mFontGlobalScale= lenght/textmaxwidth;

    heightafter = getTextHeight(fontid,_text,lenght);
    currenty += ((height-heightafter) /2.0f);
  }

  // Add all words to temp string until the box size is reach
  while ( currenttext[0]!=0 )
  {
    currentsize=0;
    stringtemp[0]=0;
    nextwordsize=getWordLenght(fontid,currenttext);
    while(    ( (currentsize + nextwordsize < lenght) && (currenttext[0]!='\\') && (currenttext[0]!=0) )
      || (  currentsize==0 && nextwordsize>=lenght ) // If the whole word do not fit, this is not a problem, display it.
      )
    {
#ifdef USE_DOUBLE_SPACE
      // Copy next word to temp string
      if ( currenttext[0]==' ' && currenttext[1]==' ')
      {
        strcat((char*)stringtemp,"  "); // This is real space so keep it.
        currenttext+=2;
      }
      else if (currenttext[0]==' ') // This is word separation, so it can be broken
      {
        currenttext++;
      }
#else
      if (currenttext[0]==' ')
      {
        strcat((char*)stringtemp," ");
        currenttext++;
      }
#endif
      while ( (currenttext[0]!=0) && (currenttext[0]!=' ') && (currenttext[0]!='\\') )
      {
        stringsmall[0]=currenttext[0];
        strcat((char*)stringtemp,(char*)stringsmall);
        currenttext++;
      }
      currentsize+=nextwordsize;
      nextwordsize=getWordLenght(fontid,currenttext);
    }
    // Display string
    drawStringCenter(fontid,x,currenty,(char*)stringtemp);
    // Go to next line
    currenty+= (font->mLineHeight*mFontGlobalScale);
    // If next caracter is special caracter then jump it
    if ( (currenttext[0]=='\\') || (currenttext[0]==' ') )
      currenttext++;
  }

  mFontGlobalScale= savescale;
}

// -----------------------------------------------------------------
// Name : drawStringInScrollBox
// -----------------------------------------------------------------
void cFontManager::drawStringInScrollBox(int fontid, int x, int y, char* _text, int xPxlLength, int yPxlLength, int scrollY)
{
  cFont* font = mAllFonts[fontid];
  unsigned char stringtemp[512];
  unsigned char stringsmall[2];
  unsigned char* currenttext;
  int   currenty;
  int   currentsize;
  float nextwordsize;
  stringsmall[1]=0;
  currenty = y;
  currenttext=(unsigned char*)_text;

  // Add all words to temp string until the box size is reach
  while ( currenttext[0]!=0 )
  {
    currentsize=0;
    stringtemp[0]=0;
    nextwordsize=getWordLenght(fontid,currenttext);
    while(    ( (currentsize + nextwordsize < xPxlLength) && (currenttext[0]!='\\') && (currenttext[0]!=0) )
      || (  currentsize==0 && nextwordsize>=xPxlLength ) // If the whole word do not fit, this is not a problem, display it.
      )
    {
#ifdef USE_DOUBLE_SPACE
      // Copy next word to temp string
      if ( currenttext[0]==' ' && currenttext[1]==' ')
      {
        strcat((char*)stringtemp," ");
        currenttext+=2;
      }
      else if (currenttext[0]==' ')
      {
        currenttext++;
      }
#else
      if (currenttext[0]==' ')
      {
        strcat((char*)stringtemp," ");
        currenttext++;
      }
#endif
      while ( (currenttext[0]!=0) && (currenttext[0]!=' ') && (currenttext[0]!='\\') )
      {
        stringsmall[0]=currenttext[0];
        strcat((char*)stringtemp,(char*)stringsmall);
        currenttext++;
      }
      currentsize+=nextwordsize;
      nextwordsize=getWordLenght(fontid,currenttext);
    }

    // Display string if in scroll box
    if (scrollY < font->mLineHeight)
    {
      drawString(fontid,x,currenty,(char*)stringtemp, scrollY, yPxlLength-(currenty-y));
      // Go to next line
      if (scrollY > 0)
        currenty += font->mLineHeight - scrollY;
      else
        currenty += font->mLineHeight;
    }

    // Return if out of scroll box
    if (currenty-y >= yPxlLength)
      break;

    scrollY -= font->mLineHeight;

    // If next caracter is special caracter then jump it
    if ( (currenttext[0]=='\\') || (currenttext[0]==' ') )
      currenttext++;
  }
}

// -----------------------------------------------------------------
// Name : getTextHeight
// -----------------------------------------------------------------
int cFontManager::getTextHeight(int fontid, char* _text, int xPxlLength)
{
  cFont* font = mAllFonts[fontid];
  unsigned char stringtemp[512];
  unsigned char stringsmall[2];
  unsigned char* currenttext;
  int   currentsize;
  float nextwordsize;
  int height = 0;
  stringsmall[1]=0;
  currenttext=(unsigned char*)_text;

  // Add all words to temp string until the box size is reach
  while ( currenttext[0]!=0 )
  {
    currentsize=0;
    stringtemp[0]=0;
    nextwordsize=getWordLenght(fontid,currenttext);
    while(    ( (currentsize + nextwordsize < xPxlLength) && (currenttext[0]!='\\') && (currenttext[0]!=0) )
      || (  currentsize==0 && nextwordsize>=xPxlLength ) // If the whole word do not fit, this is not a problem, display it.
      )
    {
#ifdef USE_DOUBLE_SPACE
      // Copy next word to temp string
      if ( currenttext[0]==' ' && currenttext[1]==' ')
      {
        strcat((char*)stringtemp," ");
        currenttext+=2;
      }
      else if (currenttext[0]==' ')
      {
        currenttext++;
      }
#else
      if (currenttext[0]==' ')
      {
        strcat((char*)stringtemp," ");
        currenttext++;
      }
#endif
      while ( (currenttext[0]!=0) && (currenttext[0]!=' ') && (currenttext[0]!='\\') )
      {
        stringsmall[0]=currenttext[0];
        strcat((char*)stringtemp,(char*)stringsmall);
        currenttext++;
      }
      currentsize+=nextwordsize;
      nextwordsize=getWordLenght(fontid,currenttext);
    }

    // Update height
    height += (font->mLineHeight*mFontGlobalScale);

    // If next caracter is special caracter then jump it
    if ( (currenttext[0]=='\\') || (currenttext[0]==' ') )
      currenttext++;
  }

  return height;
}

// -----------------------------------------------------------------
// -----------------------------------------------------------------
int cFontManager::getTextWidth(int fontid, char* _text, int xPxlLength)
{
  cFont* font = mAllFonts[fontid];
  unsigned char stringtemp[512];
  unsigned char stringsmall[2];
  unsigned char* currenttext;
  int   currentsize;
  float nextwordsize;
  stringsmall[1]=0;
  currenttext=(unsigned char*)_text;
  int bestwidth;
  int currentwidth;
  bestwidth=0;
  currentwidth=0;

  // Add all words to temp string until the box size is reach
  while ( currenttext[0]!=0 )
  {
    currentsize=0;
    stringtemp[0]=0;
    nextwordsize=getWordLenght(fontid,currenttext);
    while(    ( (currentsize + nextwordsize < xPxlLength) && (currenttext[0]!='\\') && (currenttext[0]!=0) )
      || (  currentsize==0 && nextwordsize>=xPxlLength ) // If the whole word do not fit, this is not a problem, display it.
      )
    {
#ifdef USE_DOUBLE_SPACE
      // Copy next word to temp string
      if ( currenttext[0]==' ' && currenttext[1]==' ')
      {
        strcat((char*)stringtemp," ");
        currenttext+=2;
      }
      else if (currenttext[0]==' ')
      {
        currenttext++;
      }
#else
      if (currenttext[0]==' ')
      {
        strcat((char*)stringtemp," ");
        currenttext++;
      }
#endif
      while ( (currenttext[0]!=0) && (currenttext[0]!=' ') && (currenttext[0]!='\\') )
      {
        stringsmall[0]=currenttext[0];
        strcat((char*)stringtemp,(char*)stringsmall);
        currenttext++;
      }
      currentsize+=nextwordsize;
      currentwidth+=nextwordsize;
      nextwordsize=getWordLenght(fontid,currenttext);
    }

    // Update maxwidth
    if ( currentwidth > bestwidth )
      bestwidth=currentwidth;

    currentwidth=0;

    // If next caracter is special caracter then jump it
    if ( (currenttext[0]=='\\') || (currenttext[0]==' ') )
      currenttext++;
  }

  return bestwidth;
}

// -----------------------------------------------------------------
// Name : getWordLenght
// Compute lenght of next word, including first space character if any.
// Stop when meet a space caracter, a end of line caracter '\' of end of string
// -----------------------------------------------------------------
float cFontManager::getWordLenght(int fontid, unsigned char* pString)
{
  cFont* font = mAllFonts[fontid];
  int lenght;
  int xsize = 0;

  lenght=strlen_utf8((char*)pString);

  int currenttextpos;
  currenttextpos=0;

  // Jump over first "space" character is any
  int currentcode;
  int size;
  CharDescriptor* curchar;

  currentcode = getcode_utf8( (char*) &pString[currenttextpos], &size );
  currenttextpos += size; // 1 2 3 4 (UTF 8)
  curchar = getchardescriptor(fontid, currentcode );
  lenght--;

#ifdef USE_DOUBLE_SPACE
  if ( currentcode==' ' && pString[currenttextpos]==' ')
  {
    if ( curchar->XAdvance != 0 ) xsize += curchar->XAdvance;
    else                          xsize += (font->mFontSize/3);
    currenttextpos++; // Skip second caracter
    lenght--;
  }
#else
  if ( currentcode==' ' )
  {
    if ( curchar->XAdvance != 0 ) xsize += curchar->XAdvance;
    else                          xsize += (font->mFontSize/3);
    xsize += font->mXSpacing;
  }
#endif

  // for each char of the string, add its x-size and a x-space
  for (int i=0; i<lenght; i++)
  {
    currentcode = getcode_utf8( (char*) &pString[currenttextpos], &size );
    currenttextpos += size; // 1 2 3 4 (UTF 8)
    curchar = getchardescriptor(fontid, currentcode );

    if (currentcode==' ' || currentcode=='\\' )
    {
      return xsize*mFontGlobalScale;
    }
    if (curchar->isValid)
    {
      xsize += curchar->XAdvance;
      xsize += font->mXSpacing;
    }
  }

  return xsize*mFontGlobalScale;
}

// -----------------------------------------------------------------
// Name : getStringLenght
// Compute lenght of whole string
// String can have multiple lines (delimited with \ character)
// -----------------------------------------------------------------
int cFontManager::getStringLenght(int fontid, unsigned char* pString)
{
  cFont* font = mAllFonts[fontid];
  int lenght;
  int xsize = 0;
  int bestsize=0;

  lenght=strlen_utf8((char*)pString);

  int currenttextpos;
  currenttextpos=0;

  // Jump over first "space" character is any
  int currentcode;
  int size;
  CharDescriptor* curchar;

  // for each char of the string, add its x-size and a x-space
  for (int i=0; i<lenght; i++)
  {
    currentcode = getcode_utf8( (char*) &pString[currenttextpos], &size );
    currenttextpos += size; // 1 2 3 4 (UTF 8)
    curchar = getchardescriptor(fontid, currentcode );
    // Space
#ifdef USE_DOUBLE_SPACE
    if (currentcode==' ' && pString[currenttextpos]==' ')
    {
      if ( curchar->XAdvance != 0 ) xsize += curchar->XAdvance;
      else                                   xsize += (font->mFontSize/3);
      xsize += font->mXSpacing;
      continue;
    }
    else if ( currentcode==' ' )
    {
      continue;
    }
#else
    if (currentcode==' ')
    {
      if ( curchar->XAdvance != 0 ) xsize += curchar->XAdvance;
      else                                   xsize += (font->mFontSize/3);
      xsize += font->mXSpacing;
      continue;
    }
#endif
    if (currentcode=='\\') // Go to next line
    {
      if ( xsize > bestsize )
        bestsize = xsize;
    }
    else
    {
      if (curchar->isValid)
      {
        xsize += curchar->XAdvance;
        xsize += font->mXSpacing;
      }
    }
  }

  if ( bestsize == 0 )
    return xsize*mFontGlobalScale;
  else
    return bestsize*mFontGlobalScale;
}

// -----------------------------------------------------------------
// Name : doesCharExistInFont
// Ansi function
// -----------------------------------------------------------------
bool cFontManager::doesCodeExistInFont(int fontid, int mycode)
{
  cFont* font = mAllFonts[fontid];

  CharDescriptor* curchar;
  curchar = getchardescriptor(fontid, mycode );
  if (curchar->isValid)
  {
    return true;
  }
  return false;
}

// -----------------------------------------------------------------
// Name : getFontHeight
// -----------------------------------------------------------------
int cFontManager::getFontHeight(int fontid)
{
  return mAllFonts[fontid]->mLineHeight;
}

// -----------------------------------------------------------------
// Name : drawStringCenterZoom
// Avoid Line Break
// ZoominPlace = true if the string should be Y centered at original place, else it is zommed relative to center of screen
// -----------------------------------------------------------------
void cFontManager::drawStringCenterZoom(int fontid, int x, int y, char* text, int maxwidth, int maxheight, bool zoominplace)
{
  // Compute height and lenght of text
  float textheight;
  float textwidth;

  textheight = getTextHeight(fontid, text, maxwidth);
  textwidth  = getStringLenght(fontid, (unsigned char*) text);

  if ( maxwidth/textwidth < maxheight/textheight )
    mFontGlobalScale = maxwidth/textwidth;
  else
    mFontGlobalScale = maxheight/textheight;

  // Draw string, the global scale will be considered
  float posy;
  if ( zoominplace == false )
  {
    // Zoom from screen center
    y = y - textheight/2;
    posy = (SCREENSIZEX/2.0f) - (((SCREENSIZEY/2.0f) - y )*mFontGlobalScale);
  }
  else
  {
    // Zoom from line center
    posy = (y+textheight/2.0f)-(textheight/2.0f)*mFontGlobalScale;
  }
  drawStringCenter(fontid,x,posy,text);

  mFontGlobalScale=1.0f; // Set back global scale
}


// -----------------------------------------------------------------
// Name : drawStringCenterZoom
// Avoid Line Break
// -----------------------------------------------------------------
void cFontManager::drawStringCenterZoom(int fontid, int x, int y, char* text, float zoomcoef)
{
  // Compute height and lenght of text
  float textheight;

  textheight = getTextHeight(fontid, text, SCREENSIZEX)*zoomcoef;
  y = y - textheight/2;

  mFontGlobalScale = zoomcoef;

  drawStringCenter(fontid,x,y,text);

  mFontGlobalScale=1.0f; // Set back global scale
}

//------------------------------------------------------
// Get texture and UV for one character
// (To display position under car)
//------------------------------------------------------
// Texture* cFontManager::getTextureAndUVForChar(int font, char c, float* u, float *v, float* sizeu, float* sizev)
// {
//   //ASSERT_MSG(mTextureManager, "A texture manager must be specified for the font manager!\n");
//   if ( font < 0 || font >= MAX_FONTS )
//   {
//     ASSERT_MSG(false, "Invalid font enumeration!");
//     return NULL;
//   }
// 
//   cFont*   pFont   = mAllFonts[font];
// 
//   // Batch triangles
//   Texture* pTexture;
//   pTexture = mAllFontsTextures[font];
// 
//   CharDescriptor* chardesc = getchardescriptor(font,c);
// 
//   float texturesizex;
//   float texturesizey;
//   texturesizex=pTexture->GetWidth();
//   texturesizey=pTexture->GetHeight();
// 
//   *u=chardesc->x/texturesizex;
//   *v=chardesc->y/texturesizey;
//   *sizeu=chardesc->Width/texturesizex;
//   *sizev=chardesc->Height/texturesizey;
// 
//   return pTexture;
// }
