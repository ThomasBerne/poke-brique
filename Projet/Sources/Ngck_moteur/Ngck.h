#ifndef _NGCK
#define _NGCK // Prevent multiple includes

#include <stdlib.h> // Pour le rand()
#include <string.h> // Pour le sprintf
#include <stdio.h>  
#include "f2d.h"

// -----------------------------------------------------
// TYPES
// -----------------------------------------------------

#define SCREENSIZEX 900
#define SCREENSIZEY 1000

enum eColors
{
  WHITE=0,
  RED=1,
  GREEN=2,
  BLUE=3,
  YELLOW=4,
  PURPLE=5,
  CYAN=6,
  BLACK=7,
  PURPLEDARK=8,
  MAXCOLOR=9
};

// -----------------------------------------------------
// FUNCTIONS
// -----------------------------------------------------

void KPrintSquare(int x, int y, int sizex, int sizey);
void KPrintBall(int x, int y);
void KSetDisplayColor(int colorId);
void KPrintString(char* string, int x, int y);
void KPrintStringCenter(char* string, int x, int y);
int  KGetSpace(void);
int  KGetLeft(void);
int  KGetUp(void);
int  KGetRight(void);
int  KGetDown(void);

int   KLoadSprite(char* name); // No path. Will be searched into "Data"

void  KPrintSprite(int SpriteIndex, int x, int y, float angle, float scale);
void  KPrintSpriteCenter(int SpriteId, int x, int y, float angle, float scale);
void  KPrintSpriteWithUV(int SpriteIndex, int x, int y, int ustart, int uend, int vstart, int vend, float sizex, float sizey);

void  KPrintSprite(int SpriteIndex, f2d pos, float angle, float scale);
void  KPrintSpriteCenter(int SpriteId, f2d pos, float angle, float scale);


float KRandFloat(float a, float b);
int   KRandInt(int a, int b);
float KLerp(float value_start, float value_end, float abs_start, float abs_end, float currentabs);

void  KPlaySound(char* name);
void  KPlaySound( char* name , float volume);
void  KStopSound(char* name);


#endif