#include "f2d.h"
#include "math.h"

f2d f2d::getunitvector()
{
  f2d unitvector;
  float lenght;
  lenght = sqrtf(x*x+y*y);
  unitvector = *this / lenght;
  return unitvector;
}

// Angle radian
void f2d::rotate(float angle)
{
//   if (x==0 && y==0)
//     return;
//   float oldA = getangle();
//   float newA = oldA + angle;
//   float hyp = sqrtf(x*x+y*y);
//   x = hyp*cosf(newA);
//   y = hyp*sinf(newA);
  // x' = x cos f - y sin f
  //  y' = y cos f + x sin f
  float oldx;
  float oldy;
  oldx=x;
  oldy=y;
  x=oldx*cosf(angle)-oldy*sinf(angle);
  y=oldy*cosf(angle)+oldx*sinf(angle);
}

// Calcul ma reflection par rapport au vector "other" (rebond d'un vecteur)
f2d f2d::reflect(f2d other)
{
	float velocityDotProduct = this->dot(other); //other.dot(f2d(x,y));
	f2d reflectVector = f2d(x, y) - (other * (2.0f * velocityDotProduct));
	return reflectVector;
}
