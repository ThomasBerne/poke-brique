// ------------------------------------------------------
// -- NGCK Engine
// ------------------------------------------------------

// ------------------------------------------------------
// Includes
// System includes
#include <stdio.h>
#include <stdlib.h>
#include <math.h> // cosf sqrt
#include <string.h> // string
#include <malloc.h>
#include <time.h> // time
#include <vector> // std vector
// NGCK
#include "Ngck.h"
// OpenGl
#include "gl/freeglut.h"
// Png
#include "png.h"
// Fonts
#include "FontManager.h"
// Audio
#include "AudioManager.h" // Wrapper for Fmod

#include "GL/gl.h"

// ------------------------------------------------------
// -- NGCK Extern function (user game)
// Will be called from our code
extern void GameInitialise(void);
extern void GameUpdate(float deltatime);
extern void GameDisplay(void);

// ------------------------------------------------------
// -- Customizable variables
float gClearColor[4] = { 0.4f, 0.4f, 0.4f, 0.4f }; // Clear screen color. 0 to 1.0f ? Red / Green / Blue / Unused

// ------------------------------------------------------
// -- Functions Internal prototypes
void NGCK_SetGLColor();
int  BindPNG(const char* filename, GLuint &texRef, GLuint &outWidth, GLuint &outHeight, GLint clampmode, GLint filtermode);
void GLoutputText(int x, int y, char *string);

// ------------------------------------------------------
// -- Internal Variables
// -- Sprites

// Structure for storing loaded textures info
struct sTexture
{
  int width;
  int height;
  GLuint OpenGlId;
};

using namespace std; // For Vector
vector<sTexture*> gAllTextures; // Use sprite from index

// -- Screen
//SDL_Surface*  gScreen = NULL;
//BFont_Info*   gFont = NULL;
int gCurrentColor; // ID of predefined colors (see NGCK.h)
//int done = 0;
// Style Graphique. 0=Simple
//int GfxStyle;
//long time1, time2;

// -- Keys
bool  IsRight;
bool  IsLeft;
bool  IsUp;
bool  IsDown;
bool  IsSpace;

// -------------------------------------------------------------------
// Name :
// Init of our engine
// -------------------------------------------------------------------
void NGCKInit()
{

  // Alpha blend (for png transparent)
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  // Font Manager
  cFontManager::getInstance()->Init();

  // Audio Manager
  cAudioManager::getInstance()->Init();

  srand(time(NULL));

  // Call init of user game
  GameInitialise();
}

// -------------------------------------------------------------------
// Name :
// Destroy our engine
// -------------------------------------------------------------------
void NGCKUnInit()
{
  // Delete all textures
  // TODO
}

// -------------------------------------------------------------------
// Name :
// Untextured square
// -------------------------------------------------------------------
void KPrintSquare(int x, int y, int sizex, int sizey)
{
  NGCK_SetGLColor();
  float x1,x2,y1,y2,x3,y3,x4,y4;
  x1=(float)x;
  y1=(float)y;
  x2=(float)x;
  y2=(float)y+sizey;
  x3=(float)x+sizex;
  y3=(float)y+sizey;
  x4=(float)x+sizex;
  y4=(float)y;

  NGCK_SetGLColor(); // From current color id, set glColor4f

  // Draw the front face
  glBegin( GL_QUADS);
  glVertex3f( x1, y1, 0);
  glVertex3f( x2, y2, 0);
  glVertex3f( x3, y3, 0);
  glVertex3f( x4, y4, 0);
  glEnd();
}

// -------------------------------------------------------------------
// Name :
// -------------------------------------------------------------------
void KPrintBall(int x, int y)
{
  KPrintSquare(x-10,y-10,20,20);
  //     // Test si on est dans l'ecran
  //     //if ( (x<-16) || (x>=SCREENSIZEX+16) || (y<-16) || (y>=SCREENSIZEY+16) )
  //     //  return;
  //     SDL_Color color;
  //     SDL_Rect rect;
  //     rect.x = x;
  //     rect.y = y;
  //     color = GetColorFromId(gCurrentColor);
  //     SetGlobalColor(gSpriteCircle, color);
  //     SDL_BlitSurface(gSpriteCircle, NULL, gScreen, &rect);
}

// -------------------------------------------------------------------
// Name :
// -------------------------------------------------------------------
int  KLoadSprite(char* name)
{
  string fullname;
  fullname = string("datas/");
  fullname += string(name);

  // Load texture
  GLuint texRef;
  GLuint outWidth;
  GLuint outHeight;
  GLint clampmode=GL_CLAMP; // GL_REPEAT;
  GLint filtermode=GL_LINEAR;
  int result=0;
  result = BindPNG(fullname.c_str(), texRef, outWidth, outHeight, clampmode, filtermode);

  if ( result != -1 )
  {
    sTexture* pNewTexture;
    pNewTexture = new sTexture();
    pNewTexture->width=outWidth;
    pNewTexture->height=outHeight;
    pNewTexture->OpenGlId=texRef;

    gAllTextures.push_back(pNewTexture);
    return gAllTextures.size()-1;
  }
  else
    return -1;

}

// -------------------------------------------------------------------
// Name : KPrintSprite
// Angle degres
// Scale = 1.0f normal
// -------------------------------------------------------------------
void KPrintSprite(int SpriteId, int x, int y, float angle, float scale)
{
  float spritesizex;
  float spritesizey;

  if (SpriteId==-1)
    return;

  spritesizex=gAllTextures[SpriteId]->width;
  spritesizey=gAllTextures[SpriteId]->height;
  glBindTexture(GL_TEXTURE_2D, gAllTextures[SpriteId]->OpenGlId);

  float x1,x2,y1,y2,x3,y3,x4,y4;

  float anglerad;


  f2d coords1,coords2,coords3,coords4;
  float centerx,centery;
  centerx = x + spritesizex/2.0f;
  centery = y + spritesizey/2.0f;
  // All points
  coords1.x=-spritesizex/2.0f*scale;
  coords1.y=-spritesizey/2.0f*scale;
  coords2.x=-spritesizex/2.0f*scale;
  coords2.y=spritesizey/2.0f*scale;
  coords3.x=spritesizex/2.0f*scale;
  coords3.y=spritesizey/2.0f*scale;
  coords4.x=spritesizex/2.0f*scale;
  coords4.y=-spritesizey/2.0f*scale;

  anglerad = angle * M_PI / 180.0f;
  coords1.rotate( anglerad );
  coords2.rotate( anglerad );
  coords3.rotate( anglerad );
  coords4.rotate( anglerad );

  x1 = centerx + coords1.x;
  y1 = centery + coords1.y;

  x2 = centerx + coords2.x;
  y2 = centery + coords2.y;

  x3 = centerx + coords3.x;
  y3 = centery + coords3.y;

  x4 = centerx + coords4.x;
  y4 = centery + coords4.y;

  NGCK_SetGLColor(); // From current color id, set glColor4f

  // Draw the front face
  glEnable(GL_TEXTURE_2D);
  glBegin( GL_QUADS);
  glTexCoord2f(0.0f,1.0f);
  glVertex3f( x1, y1, 0);
  glTexCoord2f(0.0f,0.0f);
  glVertex3f( x2, y2, 0);
  glTexCoord2f(1.0f,0.0f);
  glVertex3f( x3, y3, 0);
  glTexCoord2f(1.0f,1.0f);
  glVertex3f( x4, y4, 0);
  glEnd();

  glDisable(GL_TEXTURE_2D);
}

// -------------------------------------------------------------------
// -------------------------------------------------------------------
void KPrintSprite( int SpriteIndex, f2d pos, float angle, float scale )
{
  KPrintSprite(SpriteIndex,pos.x,pos.y,angle,scale);
}

// -------------------------------------------------------------------
// Name : KPrintSprite
// Angle degres
// Scale = 1.0f normal
// -------------------------------------------------------------------
void KPrintSpriteCenter(int SpriteId, int x, int y, float angle, float scale)
{
  if (SpriteId<0)
    return;
  KPrintSprite(SpriteId, x-gAllTextures[SpriteId]->width/2, y-gAllTextures[SpriteId]->height/2,  angle,  scale);
}

// -------------------------------------------------------------------
// -------------------------------------------------------------------
void KPrintSpriteCenter( int SpriteId, f2d pos, float angle, float scale )
{
  KPrintSpriteCenter(SpriteId,pos.x,pos.y,angle,scale);
}

// -------------------------------------------------------------------
// Name :
// -------------------------------------------------------------------
void KPrintSpriteWithUV( int SpriteId, int x, int y, int ustart, int uend, int vstart, int vend, float sizex, float sizey )
{
  float startu,endu,startv,endv;
  float spritesizex;
  float spritesizey;

  if (SpriteId==-1)
    return;

  spritesizex=gAllTextures[SpriteId]->width;
  spritesizey=gAllTextures[SpriteId]->height;
  glBindTexture(GL_TEXTURE_2D, gAllTextures[SpriteId]->OpenGlId);

  startu = ustart / spritesizex;
  endu = uend / spritesizex;
  startv = 1.0f - (vstart / spritesizey);
  endv = 1.0f - (vend / spritesizey);

  NGCK_SetGLColor(); // From current color id, set glColor4f

  // Draw the front face
  glEnable(GL_TEXTURE_2D);
  glBegin( GL_QUADS);
  glTexCoord2f(startu,startv);
  glVertex3f( x, y, 0);
  glTexCoord2f(startu,endv);
  glVertex3f( x, y+sizey, 0);
  glTexCoord2f(endu,endv);
  glVertex3f( x+sizex, y+sizey, 0);
  glTexCoord2f(endu,startv);
  glVertex3f( x+sizex, y, 0);
  glEnd();

  glDisable(GL_TEXTURE_2D);

  // Compute reel UV
  /*


  flipUV(&startu, &endu, &startv, &endv);

  // Draw QUAS
  BindTexture();

  // TODO
  //   glBegin(GL_QUADS);
  //   glTexCoord2f(startu,startv);
  //   glVertex2f(pos1.x,pos1.y);
  //   glTexCoord2f(startu,endv);
  //   glVertex2f(pos4.x,pos4.y);
  //   glTexCoord2f(endu,endv);
  //   glVertex2f(pos3.x,pos3.y);
  //   glTexCoord2f(endu,startv);
  //   glVertex2f(pos2.x,pos2.y);
  //   glEnd();
  */
}


// -------------------------------------------------------------------
// Name :
// -------------------------------------------------------------------
void NGCK_SetGLColor()
{
  if (gCurrentColor == WHITE) { glColor4f( 255.0f/255.0f, 255.0f/255.0f, 255.0f/255.0f, 255.0f/255.0f ); }
  else if (gCurrentColor == RED) { glColor4f( 255.0f/255.0f, 0.0f/255.0f, 0.0f/255.0f, 255.0f/255.0f ); }
  else if (gCurrentColor == GREEN)  { glColor4f( 0.0f/255.0f, 255.0f/255.0f, 0.0f/255.0f, 255.0f/255.0f ); }
  else if (gCurrentColor == BLUE)   { glColor4f( 0.0f/255.0f, 0.0f/255.0f, 255.0f/255.0f, 255.0f/255.0f ); }
  else if (gCurrentColor == YELLOW) { glColor4f( 255.0f/255.0f, 255.0f/255.0f, 0.0f/255.0f, 255.0f/255.0f ); }
  else if (gCurrentColor == PURPLE) { glColor4f( 255.0f/255.0f, 0.0f/255.0f, 255.0f/255.0f, 255.0f/255.0f ); }
  else if (gCurrentColor == PURPLEDARK) { glColor4f( 178.0f/255.0f, 0.0f/255.0f, 178.0f/255.0f, 255.0f/255.0f ); }
  else if (gCurrentColor == CYAN)   { glColor4f( 0.0f/255.0f, 255.0f/255.0f, 255.0f/255.0f, 255.0f/255.0f ); }
  else if (gCurrentColor == BLACK)   { glColor4f( 0.0f/255.0f, 0.0f/255.0f, 0.0f/255.0f, 255.0f/255.0f ); }
  else { glColor4f( 255.0f/255.0f, 255.0f/255.0f, 255.0f/255.0f, 255.0f/255.0f ); } // Default color
}

// -------------------------------------------------------------------
// Name :
// -------------------------------------------------------------------
// void ClearScreen()
// {
//   //     SDL_Color color;
//   //     SDL_Rect rect;
//   //     rect.x = 0;
//   //     rect.y = 0;
//   //     rect.w = SCREENSIZEX;
//   //     rect.h = SCREENSIZEY;
//   //     color = gClearColor;
//   //     int color32 = SDL_MapRGB(gScreen->format, color.r, color.g, color.b);
//   //     SDL_FillRect(gScreen, &rect, color32);
// }

// -------------------------------------------------------------------
// Name :
// -------------------------------------------------------------------
int KGetLeft(void)
{
  return IsLeft;
}

// -------------------------------------------------------------------
// Name :
// -------------------------------------------------------------------
int KGetUp(void)
{
  return IsUp;
}

// -------------------------------------------------------------------
// Name :
// -------------------------------------------------------------------
int KGetRight(void)
{
  return IsRight;
}

// -------------------------------------------------------------------
// Name :
// -------------------------------------------------------------------
int KGetDown(void)
{
  return IsDown;
}

// -------------------------------------------------------------------
// Name :
// -------------------------------------------------------------------
int KGetSpace(void)
{
  return IsSpace;
}

// -------------------------------------------------------------------
// Name :
// -------------------------------------------------------------------
void KSetDisplayColor(int colorid)
{
  gCurrentColor = colorid;
}

// -------------------------------------------------------------------
// Name :
// -------------------------------------------------------------------
void KPrintString(char* string, int x, int y)
{
  cFontManager::getInstance()->drawString(FONT_DEFAULT,x,y,string);
  //NGCK_SetGLColor();
  //GLoutputText(x,y,string);
  //     SDL_Color color;
  //     color = GetColorFromId(gCurrentColor);
  //     SetGlobalColor(gFont->Surface, color);
  //     PutString(gScreen, x, y - 13, string); // -8 car fonte trop grande
}

// -------------------------------------------------------------------
// Name :
// -------------------------------------------------------------------
void KPrintStringCenter(char* string, int x, int y)
{
  cFontManager::getInstance()->drawStringCenter(FONT_ORANGEJUICE,x,y,string);
  //   NGCK_SetGLColor();
  //   GLoutputText(x,y,string);
  //     SDL_Color color;
  //     color = GetColorFromId(gCurrentColor);
  //     SetGlobalColor(gFont->Surface, color);
  //     CenteredPutString(gScreen, x, y - 13, string); // -8 car fonte trop grande
}

// -----------------------------------------------------------------
// Name : RandFloat
// Random value between two values
// -----------------------------------------------------------------
float KRandFloat(float a, float b)
{
  float result;
  result = a + (((float)rand() * (b - a)) / RAND_MAX);
  return result;
}

// -----------------------------------------------------------------
// Name : RandInt
// random between a and b (a and b included)
// -----------------------------------------------------------------
int KRandInt(int a, int b)
{
  int output;
  output = a + (((int)rand() * (b + 1 - a)) / RAND_MAX);

  // Check output value
  if (output < a)
    output = a;
  if (output > b)
    output = b;

  return output;
}

// -----------------------------------------------------------
// Name : 
// abs is always froml lowest to highest
// -----------------------------------------------------------
float KLerp(float value_start, float value_end, float abs_start, float abs_end, float currentabs)
{
  if (currentabs < abs_start)
    return value_start;
  else if (currentabs > abs_end)
    return value_end;
  else
  {
    float result;
    result = value_start + (currentabs - abs_start) * (value_end - value_start) / (abs_end - abs_start);
    return result;
  }
}



// -------------------------------------------------------------------
// Name :
// -------------------------------------------------------------------
// SDL_Surface* LOAD_IMG_AND_DisplayFormat(const char *filename)
// {
//     SDL_Surface* Surftemp1;
//     SDL_Surface* Surftemp2;
//     Surftemp1 = IMG_Load(filename);
//     Surftemp2 = SDL_DisplayFormatAlpha(Surftemp1);
//     SDL_FreeSurface(Surftemp1);
//     return Surftemp2;
// }


// ----------------------------------------------------------------
// Open GL part (main entry and callbacks
// ----------------------------------------------------------------

// Prototypes
//void motionFunc(int x, int y);

// Variables
//bool gWarp = true;
// keyboard control
// bool moveForward = false;
// bool moveBackward = false;
// bool moveLeft = false;
// bool moveRight = false;
// bool keyc = false;
// bool keys = false;
// bool keyspace = false;
bool keyesc = false; // exit program with "ESC" key
// bool mousewasdown = false;
// bool mouserightwasdown = false;
// bool spacewasdown = false;
// Mouse rotation
int  MouseDeltax;
int xMouse = 0;
int yMouse = 0;
// Time
long TotalTimeSinceStart = 0;
int INTERFACE_UPDATETIME = 14; // Time asked for maximum update call (ms). 16ms = 60fps

//GLuint texture;



// -----------------------------------------------------------------
// Name : 
// -----------------------------------------------------------------
void GLmouseFunc(int button, int state, int x, int y)
{
  //printf("mouseFunc %d %d\n",x,y);

  switch (button)
  {
    // Left button
  case GLUT_LEFT_BUTTON:
    if (state == GLUT_DOWN)
    {
      //             if (mousewasdown == false)
      //             {
      //                 mousewasdown = true;
      //             }
    }
    else
      if (state == GLUT_UP)
      {
        //                 if (mousewasdown == true)
        //                 {
        //                     mousewasdown = false;
        //                 }
      }
      break;

      // Right button
  case GLUT_RIGHT_BUTTON:
    if (state == GLUT_DOWN)
    {
      //             if (mouserightwasdown == false)
      //             {
      //                 mouserightwasdown = true;
      //             }
    }
    else
      if (state == GLUT_UP)
      {
        //                 if (mouserightwasdown == true)
        //                 {
        //                     mouserightwasdown = false;
        //                 }
      }
      break;

  default:
    break;
  }
}

// -----------------------------------------------------------------
// Name : 
// -----------------------------------------------------------------
void GLmotionFunc(int x, int y)
{
  //printf("motionFunc %d %d\n",x,y);
  int dx = x - xMouse;
  xMouse = x;
  MouseDeltax = dx;
}

// -----------------------------------------------------------------
// Name : 
// -----------------------------------------------------------------
// Update and Display
void GLtimerFunc(int nValue) // Updated each 17ms
{
  //float delta;
  //delta = (float)INTERFACE_UPDATETIME / 1000.0f;
  //printf("Delta %f\n", delta);

  int deltams;
  float delta;
  deltams = glutGet(GLUT_ELAPSED_TIME) - TotalTimeSinceStart;  // glutGet = Number of milliseconds since glutInit called
  TotalTimeSinceStart = glutGet(GLUT_ELAPSED_TIME);            // Save time
  delta = deltams / 1000.0f;
  // Limit framerate (for first frame or slow computer like vista)
  if (delta > 0.100f) // Limit to 5 fps.
    delta = 0.100f;

  // Sound update
  cAudioManager::getInstance()->Update(delta);

  // Call user game update
  GameUpdate(delta);

  glutPostRedisplay();
  glutTimerFunc(INTERFACE_UPDATETIME, GLtimerFunc, 0); // LAunch timer again

  if (keyesc)
    exit(0);
}

// -----------------------------------------------------------------
// Name : 
// -----------------------------------------------------------------
void GLkeyboardFunc(unsigned char key, int x, int y)
{
  //printf("%08x\n", key);

  switch (key)
  {

    //     case 'Z':
    //     case 'z':
    //         break;
    //     case 'S':
    //     case 's':
    //         break;
    //     case 'Q':
    //     case 'q':
    //         break;
    //     case 'D':
    //     case 'd':
    //         break;
    // 
    //     case 'C':
    //     case 'c':
    //         break;

  case 32:
    IsSpace = true;
    break;

  case 27: // ESC
    keyesc = true;
#ifdef _DEBUG
    exit(0);
#endif
    break;
  }
}

// -----------------------------------------------------------------
// Name : 
// -----------------------------------------------------------------
void GLkeyboardUpFunc(unsigned char key, int x, int y)
{
  switch (key)
  {

  case 32:
    IsSpace = false;
    break;

    //     case 'Z':
    //     case 'z':
    //         break;
    //     case 'S':
    //     case 's':
    //         keys = false;
    //         break;
    //     case 'Q':
    //     case 'q':
    //         break;
    //     case 'D':
    //     case 'd':
    //         break;
    //     case 'C':
    //     case 'c':
    //         break;

  }
}

// -----------------------------------------------------------------
// Name : 
// -----------------------------------------------------------------
void GLkeyboardSpecialFunc(int key, int x, int y)
{
  switch (key)
  {
  case GLUT_KEY_UP:
    IsUp = true;
    break;
  case GLUT_KEY_DOWN:
    IsDown = true;
    break;
  case GLUT_KEY_LEFT:
    IsLeft = true;
    break;
  case GLUT_KEY_RIGHT:
    IsRight = true;
    break;

  case GLUT_KEY_F12:
    exit(0);
    break;

#ifdef _DEBUG
  case GLUT_KEY_END:
    //turbo = true;
    break;
#endif  
  }
}

// -----------------------------------------------------------------
// Name : 
// -----------------------------------------------------------------
void GLkeyboardSpecialUpFunc(int key, int x, int y)
{
  switch (key)
  {
  case GLUT_KEY_UP:
    IsUp = false;
    break;
  case GLUT_KEY_DOWN:
    IsDown = false;
    break;
  case GLUT_KEY_LEFT:
    IsLeft = false;
    break;
  case GLUT_KEY_RIGHT:
    IsRight = false;
    break;
  }
}

// -----------------------------------------------------------------
// Name : 
// -----------------------------------------------------------------
void GLdisplay(void)
{
  // update view	
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

  gluOrtho2D(0.0f, SCREENSIZEX, SCREENSIZEY, 0.0f); // l r b t

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  // clear
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glClearColor(0.5f, 0.5f, 0.5f, 1.0f);

  // Call user game display
  GameDisplay();

  // finish
  glutSwapBuffers();
}

// -----------------------------------------------------------------
// Name : GLreshapeFunc
// -----------------------------------------------------------------
void GLreshapeFunc(int w, int h)
{

  //width = w;
  //height = h;
  glViewport(0, 0, (GLsizei)w, (GLsizei)h);
}

// -----------------------------------------------------------------
// Name : 
// -----------------------------------------------------------------
void GLoutputText(int x, int y, char *string)
{
  int len, i;
  glRasterPos2f( (float) x, (float) y);
  len = (int)strlen(string);
  for (i = 0; i < len; i++)
  {
    glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, string[i]);
  }
}

// -----------------------------------------------------------------
// Name : 
// -----------------------------------------------------------------
void GLJoystick(unsigned int buttonmask, int x, int y, int z)
{
#define JOYOFFSET 400 // Out of 1000
  if (buttonmask!=0) IsSpace=true;
  else IsSpace=false;

  if (x<-JOYOFFSET) IsLeft=true;
  else IsLeft=false;

  if (x>JOYOFFSET) IsRight=true;
  else IsRight=false;

  if (y>JOYOFFSET) IsUp=true;
  else IsUp=false;

  if (y<-JOYOFFSET) IsDown=true;
  else IsDown=false;

  //printf("Joystick mask %08x, x %d y %d z %d\n", buttonmask,x,y,z);
} 

// -----------------------------------------------------------------
// Name : 
// -----------------------------------------------------------------
int	main(int argc, char **argv)
{
  printf("Debut du programme ...\n");

  glutInit(&argc, argv);

  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
  glutInitWindowSize(SCREENSIZEX, SCREENSIZEY);
  glutInitWindowPosition(450, 50); // Set window position
  glutCreateWindow("Pok�-Brique");
  // GLUT Callbacks
  glutDisplayFunc(GLdisplay); // Called at each display
  glutReshapeFunc(GLreshapeFunc); // Called when window ius resized by user
  glutMouseFunc(GLmouseFunc); // Called when mouse is clicked
  glutMotionFunc(GLmotionFunc); // Called when mouse is moved
  glutPassiveMotionFunc(GLmotionFunc); // Called when mouse is moved
  glutKeyboardFunc(GLkeyboardFunc); // Called when standard key is pressed
  glutKeyboardUpFunc(GLkeyboardUpFunc); // Called when standard key is released
  glutSpecialFunc(GLkeyboardSpecialFunc); // Called when special key is pressed
  glutSpecialUpFunc(GLkeyboardSpecialUpFunc); // Called when standard key is released
  glutTimerFunc(INTERFACE_UPDATETIME, GLtimerFunc, 0); // Called each time time of xxx ms is triggered
  glutJoystickFunc(GLJoystick, 25); // Callback each 25ms
  
  #ifndef _DEBUG
  glutSetCursor( GLUT_CURSOR_NONE );
  // glutFullScreen();
  #endif

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  srand( (unsigned int) time(NULL)); // Init rand seed

  // Init of our engine
  NGCKInit();


  /*
  GLint n;
  glGetIntegerv(GL_NUM_EXTENSIONS, &n);
  if (n > 0)
  {
    const char** extensions = (const char**)malloc(n * sizeof(char*));
    GLint i;
    for (i = 0; i < n; i++)
    {
      extensions[i] = (char*)glGetStringi(GL_EXTENSIONS, i);
    }
  }
  */


  glutMainLoop();

  // Destroy our engine
  NGCKUnInit();

  return 0;
}

// -----------------------------------------------------------------
// Name : 
// -----------------------------------------------------------------
bool LoadPNG(const char* filename, png_byte* &PNG_image_buffer, png_uint_32 &width, png_uint_32 &height)
{
  FILE *PNG_file = fopen(filename, "rb");
  if (PNG_file == NULL)
  {
    //if (!optional)
    //  sysLog.Printf("ERROR: Couldn't open %s.", filename.c_str());
    return false;
  }

  GLubyte PNG_header[8];

  fread(PNG_header, 1, 8, PNG_file);
  if (png_sig_cmp(PNG_header, 0, 8) != 0)
  {
    //if (!optional)
    //  sysLog.Printf("ERROR: %s is not a PNG.", filename.c_str());
    return false;
  }

  png_structp PNG_reader = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  if (PNG_reader == NULL)
  {
    //if (!optional)
    //  sysLog.Printf("ERROR: Can't start reading %s.", filename.c_str());
    fclose(PNG_file);
    return false;
  }

  png_infop PNG_info = png_create_info_struct(PNG_reader);
  if (PNG_info == NULL)
  {
    //if (!optional)
    //  sysLog.Printf("ERROR: Can't get info for %s.", filename.c_str());
    png_destroy_read_struct(&PNG_reader, NULL, NULL);
    fclose(PNG_file);
    return false;
  }

  png_infop PNG_end_info = png_create_info_struct(PNG_reader);
  if (PNG_end_info == NULL)
  {
    //if (!optional)
    //  sysLog.Printf("ERROR: Can't get end info for %s.", filename.c_str());
    png_destroy_read_struct(&PNG_reader, &PNG_info, NULL);
    fclose(PNG_file);
    return false;
  }

  if (setjmp(png_jmpbuf(PNG_reader)))
  {
    //if (!optional)
    //  sysLog.Printf("ERROR: Can't load %s.", filename.c_str());
    png_destroy_read_struct(&PNG_reader, &PNG_info, &PNG_end_info);
    fclose(PNG_file);
    return false;
  }

  png_init_io(PNG_reader, PNG_file);
  png_set_sig_bytes(PNG_reader, 8);

  png_read_info(PNG_reader, PNG_info);

  width = png_get_image_width(PNG_reader, PNG_info);
  height = png_get_image_height(PNG_reader, PNG_info);

  png_uint_32 bit_depth, color_type;
  bit_depth = png_get_bit_depth(PNG_reader, PNG_info);
  color_type = png_get_color_type(PNG_reader, PNG_info);

  if (color_type == PNG_COLOR_TYPE_PALETTE)
  {
    png_set_palette_to_rgb(PNG_reader);
  }

  if (color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8) 
  {
    png_set_expand_gray_1_2_4_to_8(PNG_reader);
  }

  if (color_type == PNG_COLOR_TYPE_GRAY ||
    color_type == PNG_COLOR_TYPE_GRAY_ALPHA)
  {
    png_set_gray_to_rgb(PNG_reader);
  }

  if (png_get_valid(PNG_reader, PNG_info, PNG_INFO_tRNS))
  {
    png_set_tRNS_to_alpha(PNG_reader);
  }
  else
  {
    png_set_filler(PNG_reader, 0xff, PNG_FILLER_AFTER);
  }

  if (bit_depth == 16)
  {
    png_set_strip_16(PNG_reader);
  }

  png_read_update_info(PNG_reader, PNG_info);

  PNG_image_buffer = (png_byte*)malloc(4 * width * height);
  png_byte** PNG_rows = (png_byte**)malloc(height * sizeof(png_byte*));

  unsigned int row;
  for (row = 0; row < height; ++row)
  {
    PNG_rows[height - 1 - row] = PNG_image_buffer + (row * 4 * width);
  }

  png_read_image(PNG_reader, PNG_rows);

  free(PNG_rows);

  png_destroy_read_struct(&PNG_reader, &PNG_info, &PNG_end_info);
  fclose(PNG_file);

  return true;
}

// -----------------------------------------------------------------
// Name : 
// -----------------------------------------------------------------
// adapted from SimpleImage
// http://onesadcookie.com/svn/SimpleImage/
int BindPNG(const char* filename, GLuint &texRef, GLuint &outWidth, GLuint &outHeight, GLint clampmode, GLint filtermode)
{
  png_byte* pngData;
  png_uint_32 width, height;

  if (!LoadPNG(filename, pngData, width, height))
  {
    //error was output by the load
    return -1;
  }

  outWidth = width;
  outHeight = height;

  glGenTextures(1, &texRef);
  glBindTexture(GL_TEXTURE_2D, texRef);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, clampmode);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, clampmode);

  glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filtermode);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filtermode);

  glTexImage2D(
    GL_TEXTURE_2D,
    0, 
    GL_RGBA,
    width,
    height,
    0,
    GL_RGBA,
    GL_UNSIGNED_BYTE,
    pngData
    );

  free(pngData);

  return 0;
}

// -----------------------------------------------------------------
// Name : 
// -----------------------------------------------------------------
void KPlaySound( char* name )
{
  cAudioManager::getInstance()->PlaySound(name);
}

// -----------------------------------------------------------------
// Name : 
// volume 0 to 1.0f
// -----------------------------------------------------------------
void KPlaySound( char* name , float volume)
{
  cAudioManager::getInstance()->PlaySound(name,f2d(0,0),volume);
}

// -----------------------------------------------------------------
// Name : 
// -----------------------------------------------------------------
void KStopSound( char* name )
{
  cAudioManager::getInstance()->StopSound(name);
}


