#ifndef _AUDIOMANAGERH
#define _AUDIOMANAGERH

//#include "Common.h"
#include "Fmod/inc/fmod.hpp"
#include "Fmod/inc/fmod_errors.h"
#include "f2d.h"
#include "Ngck.h"

class cAudioManager  
{
public:
  ~cAudioManager();

	void           Init();
	void           Update(float delta);

  FMOD::Channel* PlaySound(char* name, f2d pos, float volume=1.0f);
  float          GetSoundLength( char* name );
  FMOD::Channel* PlaySound(char* name);

  void           StopSound(char* name);

  void           PauseAllSounds();
  void           UnPauseAllSounds(); 

  void           PlaySoundWithDelay(char* name, f2d pos, float delay);
  float          GetSoundLengthWithDelay( char* name );

  int            GetNbOfsounds();
  char*          GetSoundName(int index);

  void           SetListenerPos(f2d pos);
  void           SetListenerOrientation(f2d pos);

  void           SetReverbLevel(int level);

public:
  static cAudioManager* getInstance() { if (!mInstance) mInstance = new cAudioManager; return mInstance; }

private:

	cAudioManager();
  bool   LoadSound(char* name, int currentindex, bool flagloop, bool flag2d);
  int    FindIndexFromName(char* name);
  void   NameGetFlags(char* name, bool* flagalt, bool* flagloop, bool* flag2D);
  bool   IsValidSoundFile(char* name);

  static cAudioManager* mInstance;

  f2d      mListenerPos;
  f2d      mListenerOrientation;

  //FMOD::Geometry* mGeometry; // Occluder
  //FMOD::Geometry* mGeometry2; // Occluder Room

//#define MAXSOUNDWITHDELAY 5

  //char  mSoundWithDelayName[MAXSOUNDWITHDELAY][128]; // Store name of sound to be played
  //float mSoundWithDelayValues[MAXSOUNDWITHDELAY]; // <= 0.0f inactive, >0 active
  //f2d   mSoundWithDelayPositions[MAXSOUNDWITHDELAY];

};

#endif
