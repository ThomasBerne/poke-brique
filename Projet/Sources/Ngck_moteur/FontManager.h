#ifndef _FontManager_H
#define _FontManager_H

#include "NGCK.h"

//#include "Texture.h"

#define FONT_DEFAULT          0
#define FONT_ORANGEJUICE      1

//#define FONT_STONEHINGE_24    0 // SmallFont
//#define FONT_STONEHINGE_32    1 // BaseFont
//#define FONT_STONEHINGE_128   2 // Big Font (64 pixels high)


#define MAX_FONTS             2

// -----------------------------------------------------------------
// CharDescriptor. a struct to store 1 graphic character
struct CharDescriptor
{
  unsigned int  id;
  short         x, y;
  short         Width, Height;
  signed short  XOffset, YOffset;
  short         XAdvance;
  bool          isValid;
  CharDescriptor() : id(-1), x( 0 ), y( 0 ), Width( 0 ), Height( 0 ), XOffset( 0 ), YOffset( 0 ), XAdvance( 0 ), isValid(false) { };
};

// -----------------------------------------------------------------
// Store one Font

#define FONT_MAXCHAR 1024

class cFont
{
public:
  cFont() {};
  ~cFont() {};
public:
  int            mId;
  //int            mTextureIdx;
  unsigned short mLineHeight;
  unsigned short mWidth, mHeight;
  unsigned short mFontSize;
  short          mXSpacing; // Space between caracters
  int            mTextureId; // Id of associated texture
  CharDescriptor mChars[FONT_MAXCHAR];
};


// -----------------------------------------------------------------
class cFontManager
{
public:
  ~cFontManager();
  static cFontManager * getInstance()
  {
    if (mInstance == NULL) mInstance = new cFontManager(); return mInstance;
  };

  void Init();
  void Destroy();

  void drawString(int fontid, int x, int y, char* text);
  void drawStringCenter(int fontid, int x, int y, char* text);
  void drawStringRight(int fontid, int x, int y, char* text);
  void drawStringInBox(int fontid, int x, int y, char* text, int lenght); // Draw words until box size is reached and then go to next line and continue display
  void drawStringCenterInBox(int fontid, int x, int y, char* text, int lenght); // Draw words until box size is reached and then go to next line and continue display
  void drawStringInScrollBox(int fontid, int x, int y, char* _text, int xPxlLength, int yPxlLength, int scrollY);
  void drawStringCenterZoom(int fontid, int x, int y, char* text, int maxwidth, int maxheight, bool zoominplace=false); // Zoom the text to best fit the indicated size
  void drawStringCenterZoom(int fontid, int x, int y, char* text, float zoomcoef);

  int  getFontHeight(int fontid);
  int  getTextHeight(int fontid, char* _text, int xPxlLength);
  int  getTextWidth(int fontid, char* _text, int xPxlLength);
  float getWordLenght(int fontid, unsigned char* pString);
  int  getStringLenght(int fontid, unsigned char* pString);
  bool doesCodeExistInFont(int fontid, int mycode);

  void unloadAll();
  
  void SetColor(float r,float g, float b) { mColorRed=r; mColorGreen=g; mColorBlue=b; };
  void SetAlpha(float a) { mAlpha=a;}; // 0 to 255.0f

  int strlen_utf8(char* text);

  void SetScale(float value) { mFontGlobalScale=value; };

  //Texture* getTextureAndUVForChar(int font, char c, float* u, float *v, float* sizeu, float* sizev);

private:
  cFontManager();
  static cFontManager * mInstance;

  bool CreateFont(int fontid, char* fontdefname);
  void drawString(int fontid, int x, int y, char* text, int yStart, int yEnd);
  int  BMFont_FindBlock(int id, FILE* file);
  int  getcode_utf8(char* text, int* size);
  CharDescriptor* getchardescriptor(int fontid, int code);

  cFont*     mAllFonts[MAX_FONTS];
  //Texture*   mAllFontsTextures[MAX_FONTS];

  float     mColorRed;
  float     mColorGreen;
  float     mColorBlue;
  float     mAlpha;

  float     mFontGlobalScale; // This scale is applied to all functions (1.0f = no change, 2.0f = double size)
};

#endif
