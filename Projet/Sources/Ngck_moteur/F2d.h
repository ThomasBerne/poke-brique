#ifndef F2DINC
#define F2DINC

// Point or vector 2D

#include "math.h"

#define M_PI 3.14159265f


class f2d
{
public:
  float x;
  float y;
  f2d() {x=0.0f; y=0.0f;};
  f2d(float _x, float _y) {x=_x; y=_y;};
  void operator=(f2d op2) {x=op2.x; y=op2.y;};
  bool operator==(f2d op2) {return (x==op2.x && y==op2.y);};
  bool operator!=(f2d op2) {return (x!=op2.x || y!=op2.y);};
  f2d operator+(f2d op2) { return f2d(x+op2.x, y+op2.y); };
  f2d operator-(f2d op2) { return f2d(x-op2.x, y-op2.y); };
  f2d operator-(float scale) { return f2d(x-scale, y-scale); };
  f2d operator*(int scale) { return f2d(x*scale, y*scale); };
  f2d operator*(float scale) { return f2d(x*scale, y*scale); };
  f2d operator/(float scale) { return f2d(x/scale, y/scale); };
  f2d operator/(f2d op2) { return f2d(x/op2.x, y/op2.y); };
  void operator+=(f2d op2) {x+=op2.x; y+=op2.y;};
  void operator+=(float scale) {x+=scale; y+=scale;};
  void operator-=(f2d op2) {x-=op2.x; y-=op2.y;};
  void operator-=(float scale) {x-=scale; y-=scale;};
  void operator*=(f2d op2) {x*=op2.x; y*=op2.y;};
  void operator*=(float scale) {x*=scale; y*=scale;};
  void operator/=(f2d op2) {x/=op2.x; y/=op2.y;};
  void operator/=(float scale) {x/=scale; y/=scale;};
  float distsquared(f2d op2) { return (op2.x-x)*(op2.x-x) + (op2.y-y)*(op2.y-y); };
  float dist(f2d op2) { return sqrtf(distsquared(op2)); };
  float getsize() {return sqrtf(x*x+y*y); };
  // getangle return a [0;2Pi[ value
  float getangle() {if (y>=0) return acosf(getunitvector().x);
                    else return 2.0f*(float)M_PI-acosf(getunitvector().x); };
  // setfromangle, [0;2Pi[
  void  setfromangle(float angle) { x=cosf(angle); y=sinf(angle); };
  f2d   getunitvector();
  void rotate(float angle);
  void normalize() { float size=getsize(); if (size==0.0f) return; x=x/size; y=y/size; };
  float dot(f2d op2) { return ( x * op2.x + y * op2.y ); };// Dot product (vector projection on another vector)
  f2d  getnormal() { return f2d(-y,x); }; // Return normal vector (90degres rotation)

  bool isleft(f2d c){ return ( (x - 0.0f)*(c.y - 0.0f) - (y - 0.0f)*(c.x - 0.0f) ) > 0.0f; };
  f2d reflect(f2d other);
};

#endif