
#include "Functions.h"


vector<Balle> balles;
Barre barre;
vector<Brique> briques;
vector<GameEvent> gameEvents;

const int VAGUE_MAX = 10;
bool perdu, gagne, superBalle;
int scoreJeu, vies, spriteBalle, spriteBarre, backgroundSprite, vague;
std::map<std::string, int> spriteBrique;
float deltatimeGlobal;

#pragma region Fonctions_de_support

//Convertit une chaine de caract�re de std::string en tableau de char (char*)
char* toCharArray(std::string str) {
	char *cstr = new char[str.length() + 1];
	strcpy(cstr, str.c_str());
	return cstr;
}
// Recentre la valeur x entre les valeurs inMin et inMax sur l'�chelle de outMin et outMax
// (par exemple x = 40 entre 0 et 100, retournera 20 si l'�chelle de sortie est de 0 � 25 et 80 si l'�chelle de sortie est entre 0 et 200)
float mapping(float x, float inMin, float inMax, float outMin, float outMax) {
	return (x - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
}
// Fonction qui retourne la valeur la plus proche de value mais qui doit �tre entre min et max
template<class T>
T clamp(T value, T min, T max)
{
	if (value <= min) {
		return min;
	}
	else if (value >= max) {
		return max;
	}
	else {
		return value;
	}
}
// Fonction d'initialisation des sprites
void initialisationSprites() {
	// Sprite du background
	backgroundSprite = KLoadSprite("background.png");

	// Sprite de la balle
	spriteBalle = KLoadSprite("pokeball.png");

	// Sprite des briques
	int spriteNum = KLoadSprite("Bulbasaur.png");
	spriteBrique.insert(std::pair<std::string, int>("Bulbasaur", spriteNum));
	spriteNum = KLoadSprite("Pikachu.png");
	spriteBrique.insert(std::pair<std::string, int>("Pikachu", spriteNum));
	spriteNum = KLoadSprite("Evoli.png");
	spriteBrique.insert(std::pair<std::string, int>("Evoli", spriteNum));
	spriteNum = KLoadSprite("Blastoise.png");
	spriteBrique.insert(std::pair<std::string, int>("Blastoise", spriteNum));
	spriteNum = KLoadSprite("Charizard.png");
	spriteBrique.insert(std::pair<std::string, int>("Charizard", spriteNum));
	spriteNum = KLoadSprite("Mew.png");
	spriteBrique.insert(std::pair<std::string, int>("Mew", spriteNum));

	// Sprite de la barre
	spriteBarre = KLoadSprite("barre.png");
}

#pragma endregion

#pragma region Fonctions_de_gestion_des_collisions

void collisionMurs(Balle& balle, int index)
{
	if (balle.pos.x - 15 <= 0) {
		balle.dir.x = abs(balle.dir.x);
	}
	if (balle.pos.x + 15 >= SCREENSIZEX) {
		balle.dir.x = -abs(balle.dir.x);
	}
	if (balle.pos.y - 15 <= 30) {
		balle.dir.y = abs(balle.dir.y);
	}
	if (balle.pos.y + 15 >= SCREENSIZEY) {
		balle.dir.y = -abs(balle.dir.y);
		--vies;
		perdu = vies <= 0;
		if (balles.size() == 1) {
			balle.pos.x = SCREENSIZEX / 2;
			balle.pos.y = SCREENSIZEY / 2 + 200;
		}
		else {
			balles.erase(balles.begin() + index);
		}
	}
}
bool lineLine(f2d p1, f2d p2, f2d p3, f2d p4)
{

	// Calcul la direction des droites
	float uA = ((p4.x - p3.x)*(p1.y - p3.y) - (p4.y - p3.y)*(p1.x - p3.x)) / ((p4.y - p3.y)*(p2.x - p1.x) - (p4.x - p3.x)*(p2.y - p1.y));
	float uB = ((p2.x - p1.x)*(p1.y - p3.y) - (p2.y - p1.y)*(p1.x - p3.x)) / ((p4.y - p3.y)*(p2.x - p1.x) - (p4.x - p3.x)*(p2.y - p1.y));

	// Si les droites sont en collisions
	return uA >= 0 && uA <= 1 && uB >= 0 && uB <= 1;
}

std::string collisionBalleBriques(Balle& balle)
{
	for (int i = (int)briques.size() - 1; i >= 0; i--)
	{
		// Calcul du point le plus proche de la balle
		f2d nearestPoint;
		nearestPoint.x = clamp(balle.pos.x, briques[i].pos.x - 50, briques[i].pos.x + 50);
		nearestPoint.y = clamp(balle.pos.y, briques[i].pos.y - 15, briques[i].pos.y + 15);

		if (checkCollisionBrique(balle, briques[i])) {
			scoreJeu += briques[i].points;
			--briques[i].vies;

			if (!superBalle) {
				f2d vectorNormal;
				vectorNormal.x = nearestPoint.x - balle.pos.x;
				vectorNormal.y = nearestPoint.y - balle.pos.y;
				vectorNormal.normalize();
				balle.dir = balle.dir.reflect(vectorNormal);
			}

			if (briques[i].vies == 0 || superBalle) {
				std::string typeBrique = briques[i].type;
				briques.erase(briques.begin() + i);
				KPlaySound("brique-detruite", 100.0f);
				if (briques.size() == 0) {
					gagne = true;
				}
				return typeBrique;
			}
			else {
				KPlaySound("brique-touchee", 100.0f);
			}
		}
	}
	return "";
}

bool checkCollisionBrique(Balle balle, Brique brique)
{

	// Calcul la prochaine position de la balle pour la collision
	f2d nextPos = balle.pos + balle.dir * balle.vitesseBalle * deltatimeGlobal;

	// Calcul la position des bords de la brique.
	f2d coin1, coin2, coinInfDroit, coinInfGauche, coinSupDroit, coinSupGauche;
	coinInfDroit.x = brique.pos.x + 50.0f;
	coinInfDroit.y = brique.pos.y + 15.0f;
	coinInfGauche.x = brique.pos.x - 50.0f;
	coinInfGauche.y = brique.pos.y + 15.0f;
	coinSupDroit.x = brique.pos.x + 50.0f;
	coinSupDroit.y = brique.pos.y - 15.0f;
	coinSupGauche.x = brique.pos.x - 50.0f;
	coinSupGauche.y = brique.pos.y - 15.0f;

	bool collision = false;

	coin1.x = brique.pos.x + 50.0f;
	coin1.y = brique.pos.y + 30.0f;
	coin2.x = brique.pos.x - 50.0f;
	coin2.y = brique.pos.y + 30.0f;
	collision = collision || lineLine(balle.pos, nextPos, coin1, coin2);  // Surface inf�rieure

	coin1.x = brique.pos.x + 65.0f;
	coin1.y = brique.pos.y + 15.0f;
	coin2.x = brique.pos.x + 65.0f;
	coin2.y = brique.pos.y - 15.0f;
	collision = collision || lineLine(balle.pos, nextPos, coin1, coin2);   // Surface droite

	coin1.x = brique.pos.x - 50.0f;
	coin1.y = brique.pos.y - 30.0f;
	coin2.x = brique.pos.x + 50.0f;
	coin2.y = brique.pos.y - 30.0f;
	collision = collision || lineLine(balle.pos, nextPos, coin1, coin2);  // Surface sup�rieure

	coin1.x = brique.pos.x - 65.0f;
	coin1.y = brique.pos.y + 15.0f;
	coin2.x = brique.pos.x - 65.0f;
	coin2.y = brique.pos.y - 15.0f;
	collision = collision || lineLine(balle.pos, nextPos, coin1, coin2); // Surface gauche

	collision = collision || balle.pos.dist(coinInfDroit) < 15.0f;
	collision = collision || balle.pos.dist(coinInfGauche) < 15.0f;
	collision = collision || balle.pos.dist(coinSupDroit) < 15.0f;
	collision = collision || balle.pos.dist(coinSupGauche) < 15.0f;

	return collision;
}

void collisionBalleBarre(Balle& balle)
{
	float tailleLargeur = (200.0f * barre.multiplicateur);
	float tailleHauteur = (5.0f * barre.multiplicateur);

	float barreLeft = barre.pos.x - (tailleLargeur / 2.0f);
	float barreRight = barre.pos.x + (tailleLargeur / 2.0f);
	float posX = clamp(balle.pos.x, barreLeft, barreRight);
	float dirXRedirection = (posX - (barre.pos.x - (tailleLargeur / 2.0f)));
	dirXRedirection = mapping(dirXRedirection, 0.0f, tailleLargeur, -0.85f, 0.85f);

	if (balle.pos.y > barre.pos.y - tailleHauteur / 2.0f) {
		if (balle.pos.x + 15.0f >= barreLeft
			&& balle.pos.x - 15.0f <= barreRight) {
			balle.dir.x = -balle.dir.x;
		}
	}
	else if (balle.pos.x + 15.0f >= barreLeft
		&& balle.pos.x - 15.0f <= barreRight
		&& balle.pos.y + 15.0f > barre.pos.y - tailleHauteur / 2.0f) {

		balle.dir.x = dirXRedirection;
		balle.dir.y = -abs(-sin(acos(dirXRedirection)));
		balle.dir.normalize();

		KPlaySound("ball-bouncing", 100.0f);


	}
}
#pragma endregion

#pragma region Fonctions_de_mise_�_jour_de_l_interface
// Mets � jour l'affichage des balles.
void updateBalles()
{
	for (int i = 0; i < balles.size(); i++)
	{
		Balle* b = &balles[i];
		(*b).pos = (*b).pos + (*b).dir * (*b).vitesseBalle * deltatimeGlobal;
	}
}

void gestionEventProchaineManche() {
	if (gagne) {
		gameEvents.erase(gameEvents.begin(), gameEvents.end());
		balles.erase(balles.begin(), balles.end());
		Balle b;
		balles.push_back(b);
		barre.reset();
		++vague;
		gagne = false;
	}
	if (KGetSpace()) {
		if (vague <= 5) {
			briques = peuplerCarte(4);
		}
		else if (vague <= 6) {
			briques = peuplerCarte(5);
		}
		else if (vague <= VAGUE_MAX - 1) {
			briques = peuplerCarte(6);
		}
		else {
			briques = peuplerCarte(8);
		}
	}
}
void majLevel() {
	barre.deplacementBarre();
	updateBalles();
	for (int i = (int)balles.size() - 1; i >= 0; i--)
	{
		collisionBalleBarre(balles[i]);

		string typeBrique = collisionBalleBriques(balles[i]);
		if (typeBrique != "") {
			GameEvent gEvent;
			gEvent.timer = 0.0f;
			gEvent.type = typeBrique;
			if (gEvent.type == "Evoli") {
				for (int j = (int)gameEvents.size() - 1; j >= 0; j--)
				{
					if (gameEvents[j].type == "Evoli") {
						gameEvents.erase(gameEvents.begin() + j);
					}
				}
			}
			gameEvents.push_back(gEvent);
		}
		collisionMurs(balles[i], i);
	}
	for (int i = (int)gameEvents.size() - 1; i >= 0; i--)
	{
		gestionEvent(gameEvents[i], i);
	}
}

void finEchec() {
	gameEvents.erase(gameEvents.begin(), gameEvents.end());
	balles.erase(balles.begin(), balles.end());
	Balle b;
	balles.push_back(b);
	barre.reset();
	vague = 1;
	vies = 3;
	superBalle = false;
	if (KGetSpace()) {
		scoreJeu = 0;
		perdu = false;
	}
}

void finSucces() {
	gameEvents.erase(gameEvents.begin(), gameEvents.end());
	balles.erase(balles.begin(), balles.end());
	Balle b;
	balles.push_back(b);
	barre.reset();
	vies = 3;
	superBalle = false;
	if (KGetSpace()) {
		briques = peuplerCarte(4);
		vague = 1;
		scoreJeu = 0;
		gagne = false;
	}
}
#pragma endregion

#pragma region Fonctions_d_affichage
// Affiche la liste des balles
void afficherBalles()
{
	for (int i = 0; i < balles.size(); i++)
	{
		KPrintSpriteCenter(spriteBalle, balles[i].pos, 0.0f, 0.125f);
	}
}
vector<Brique> peuplerCarte(int ranges)
{
	vector<Brique> briques;
	for (int y = 100; y < ranges * 100; y += 50)
	{
		int start = SCREENSIZEX % 110 / 2 + 50;
		int end = SCREENSIZEX - (SCREENSIZEX % 110 / 2) - 50;
		for (int x = start; x < end; x += 110)
		{
			Brique b(x, y);
			briques.push_back(b);
		}
	}
	return briques;
}

void afficherbarreScore()
{
	KSetDisplayColor(BLACK);
	KPrintSquare(0, 0, SCREENSIZEX, 30);
	KSetDisplayColor(WHITE);

	std::string strScore = "Score : " + std::to_string(scoreJeu);
	char* cstr = toCharArray(strScore);
	KPrintString(cstr, 0, 0);

	strScore = "Vies : " + std::to_string(vies);
	cstr = toCharArray(strScore);
	KPrintString(cstr, 250, 0);
}

void afficherBriques()
{
	for (int i = 0; i < briques.size(); i++)
	{
		KPrintSpriteCenter(spriteBrique[briques[i].type], briques[i].pos, 0.0f, 1.0f);
	}
}

void affichageProchaineMancheMenu() {
	KPrintSprite(backgroundSprite, 0, 0, 0, 1.0f);
	if (vague <= 1) {
		KPrintStringCenter("Bienvenue sur PokeBrique !", SCREENSIZEX / 2, SCREENSIZEY / 2 - 50);
		KPrintStringCenter("Pour commencer appuyez sur SPACE !", SCREENSIZEX / 2, SCREENSIZEY / 2 + 50);
	}
	else {
		string str = "Vous avez fini la vague " + to_string(vague - 1) + " / " + to_string(VAGUE_MAX) + " !";
		
		KPrintStringCenter(toCharArray(str), SCREENSIZEX / 2, SCREENSIZEY / 2 - 50);
		str = "Pour commencer la vague " + to_string(vague);
		KPrintStringCenter(toCharArray(str), SCREENSIZEX / 2, SCREENSIZEY / 2 + 50);
		KPrintStringCenter("appuyez sur SPACE", SCREENSIZEX / 2, SCREENSIZEY / 2 + 150);

	}
}

void affichageEchec() {
	KPrintSprite(backgroundSprite, 0, 0, 0, 1.0f);
	KPrintStringCenter("Vous avez perdu !", SCREENSIZEX / 2, SCREENSIZEY / 2 - 100);

	string str = "Votre score : " + to_string(scoreJeu);
	KPrintStringCenter(toCharArray(str), SCREENSIZEX / 2, SCREENSIZEY / 2);
	KPrintStringCenter("Pour recommencer appuyez sur SPACE !", SCREENSIZEX / 2, SCREENSIZEY / 2 + 100);
}

void affichageSucces() {
	KPrintSprite(backgroundSprite, 0, 0, 0, 1.0f);
	KPrintStringCenter("Vous avez Gagne !", SCREENSIZEX / 2, SCREENSIZEY / 2 - 100);

	string str = "Votre score : " + to_string(scoreJeu);
	KPrintStringCenter(toCharArray(str), SCREENSIZEX / 2, SCREENSIZEY / 2);
	KPrintStringCenter("Pour recommencer appuyez sur SPACE !", SCREENSIZEX / 2, SCREENSIZEY / 2 + 100);
}
#pragma endregion

#pragma region Fonctions_d_ajout_de_feature
// Gestion des �v�nements si 
void gestionEvent(GameEvent& gEvent, int index)
{
	std::cout << gEvent.type << " : " << gEvent.timer << endl;
	// std::cout << "global Timer : " << deltatimeGlobal << endl;
	gEvent.timer += deltatimeGlobal;
	if (gEvent.type == "Bulbasaur") {
		if (gEvent.timer > 0.0f) {
			gameEvents.erase(gameEvents.begin() + index);
		}

	}
	else if (gEvent.type == "Pikachu") {
		if (gEvent.timer > 0.0f) {
			gameEvents.erase(gameEvents.begin() + index);
		}

	}
	else if (gEvent.type == "Evoli") {
		if (gEvent.timer == deltatimeGlobal) {
			barre.multiplicateur = 1.5f;
		}
		if (gEvent.timer > 10.0f) {
			barre.multiplicateur = 0.5f;
		}
		if (gEvent.timer > 15.0f) {
			gameEvents.erase(gameEvents.begin() + index);
			barre.multiplicateur = 1.0f;
		}

		if (gEvent.timer < 9.0f) {
			barre.etat = "grand";
		}
		if (gEvent.timer > 9.0f && gEvent.timer < 14.0f) {
			barre.etat = "petit";
		}
		if (gEvent.timer > 14.0f) {
			barre.etat = "normal";
		}
	}
	else if (gEvent.type == "Blastoise") {
		if (gEvent.timer == deltatimeGlobal) {
			if (balles.size() < 3) {
				Balle b;
				balles.push_back(b);
			}
		}
		else if (gEvent.timer > 25.0f) {
			gameEvents.erase(gameEvents.begin() + index);
			if (balles.size() > 1) {
				balles.erase(balles.begin() + 1, balles.begin() + 2);
			}
		}
	}
	else if (gEvent.type == "Charizard") {
		if (gEvent.timer == deltatimeGlobal) {
			for (int i = 0; i < balles.size(); i++) {
				balles[i].vitesseBalle = 500.0f;
			}
		}
		else if (gEvent.timer > 25.0f) {
			gameEvents.erase(gameEvents.begin() + index);
			for (int i = 0; i < balles.size(); i++) {
				balles[i].vitesseBalle = 300.0f;
			}
		}

	}
	else { // Type = "Mew"
		if (gEvent.timer == deltatimeGlobal) {
			vies = vies + 3;
			superBalle = true;
		}
		else if (gEvent.timer > 10.0f) {
			gameEvents.erase(gameEvents.begin() + index);
			superBalle = false;
		}
	}
}

#pragma endregion






#pragma region Fonctions_d_init_update_et_display
void GameInitialise()
{
	// Initialisation du random
	srand(time(0));

	// Initialisation des variables de victoire et d'�chec de la partie.
	perdu = gagne = superBalle = false;
	scoreJeu = 0;
	vague = 1;
	vies = 3;

	// Cr�ation de la premi�re balle
	Balle b;
	balles.push_back(b);

	// Initialisation des sprites
	initialisationSprites();

	KPlaySound("pokemon-theme", 0.03f);
}

void GameUpdate(float deltatime)
{
	if (KGetUp()) {
		superBalle = true;
	}
	if (KGetDown()) {
		superBalle = false;
	}

	deltatimeGlobal = deltatime;
	if (perdu) {
		finEchec();
	}
	else if (briques.size() == 0 && vague == VAGUE_MAX) {
		finSucces();
	}
	else if (briques.size() == 0 && vague < VAGUE_MAX) {
		gestionEventProchaineManche();
	}
	else {
		majLevel();
	}
}

void GameDisplay()
{
	if (perdu) {
		affichageEchec();
	}
	else if (briques.size() == 0 && vague == VAGUE_MAX) {
		affichageSucces();
	}
	else if (briques.size() == 0 && vague < VAGUE_MAX) {
		affichageProchaineMancheMenu();
	}
	else {
		KPrintSprite(backgroundSprite, 0, 0, 0, 1.0f);
		barre.affichageBarre(spriteBarre);
		afficherBalles();
		afficherBriques();
		afficherbarreScore();
	}
}

#pragma endregion