#pragma once

#include "Balle.h"
#include "Barre.h"
#include "Brique.h"
#include <iostream>
#include<string>

using namespace std;

struct GameEvent
{
	string type;
	float timer;
};
char* toCharArray(std::string str);
void updateBalles();
void afficherBalles();
template<class T>
T clamp(T value, T min, T max);
float mapping(float x, float inMin, float inMax, float outMin, float outMax);
void gestionEvent(GameEvent& typeBrique, int index);

bool lineLine(f2d p1, f2d p2, f2d p3, f2d p4);
std::string collisionBalleBriques(Balle& balle);
bool checkCollisionBrique(Balle balle, Brique brique);
void collisionMurs(Balle& balle, int index);

void collisionBalleBarre(Balle& balle);
void afficherBriques();
vector<Brique> peuplerCarte(int ranges);

void afficherbarreScore();
void initialisationSprites();
void finEchec();
void majLevel();
void gestionEventProchaineManche();
void affichageProchaineMancheMenu();
void finSucces();
void affichageSucces();

