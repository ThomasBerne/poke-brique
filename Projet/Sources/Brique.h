#pragma once

#include "Ngck_moteur/Ngck.h"
#include <stdlib.h>
#include <vector>
#include <time.h>

class Brique
{
public:
	f2d pos;
	std::string type;
	int points;
	int vies;

	Brique();
	Brique::Brique(int posX, int posY);
	~Brique();
};

