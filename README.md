Poké-Briques :
Jeu de casse briques avec un style pokémon
où le but est de casser les briques des pokémons pour gagner des points

Brique Bulbizarre (100 points) :
1 vie
Aucun effet
Brique Pikachu (200 points) :
1 vie
Aucun effet
Brique Evoli (200 points) : 
2 vies
Augmente la taille de la barre pendant 10 secondes puis la réduit pendant 5 secondes
Brique Tortank (400 points) : 
3 vies
Ajoute une nouvelle balle jusqu'à un maximum de 3. Elles disparaissent pendant 25 secondes
Brique Dracaufeu (400 points) :
3 vies
Augmente la vitesse des balles pendant 25 secondes
Brique Mew (1000 points) :
4 vies
Ajoute 3 vies au joueur